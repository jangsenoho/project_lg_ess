var currentEmailCheck = "Y";
var currentPasswordCheck = "Y";

var memberEmail1 = "N";
var memberEmail2 = "N";
var memberPassword1 = "N";
var memberPassword2 = "N";
var businessType = "N";
var companyName = "N";
var ceoName = "N";
var employeeCount = "N";
var companyPhone = "N";
var companyAddress = "N";
var companyCity = "N";
var companyZipcode = "N";
var preferredDistributor1 = "N";
var preferredDistributor2 = "N";

var memberEmailCheck = "N";
var companyPhoneCheck = "N";
var companyCheck = "N";

// 영문 숫자 포함 여부 확인
var isU = /[a-z]/;
var isL = /[A-Z]/;
var isN = /[0-9]/;
var isPhone = /[^0-9]/g;
var isID = /^[a-zA-Z0-9][a-zA-Z0-9_\-\.]{0,20}$/;
var isEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,20}))$/;
var isHan = /[ㄱ-ㅎ가-힣]/g;

function inputMsgHide(obj) {
	$("#"+obj+"Msg").html("");
	$("#"+obj+"Msg").hide();
}

function inputMsgShow(obj, objText) {	
	$("#"+obj+"Msg").html(objText);
	$("#"+obj+"Msg").show();
}

function checkForm(formId, updateValue) {
	
	if (formId == "memberEmail1") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error show\">E-Mail ID eingeben</div>");
			currentEmailCheck = "Y";
			return 'N';
		} else {
			currentEmailCheck = "N";
			//inputMsgHide(formId);
		}
		// 영문, 숫자 이외의 글자 확인
		if (!isID.test($("#"+formId).val())) {
			inputMsgShow(formId, "<div class=\"error show\">1-20 of alphbet, numbers and special characters (_), (-) is only available.</div>");
			currentEmailCheck = "Y";
			return 'N';
		} else {
			currentEmailCheck = "N";
			inputMsgHide(formId);
		}
		
		memberEmail1 = "Y";
	}
	
	if (formId == "memberEmail2") {
		// 입력 여부 확인
		if (currentEmailCheck == "N") {
			if ($("#"+formId).val() == "") {
				inputMsgShow("memberEmail1", "<div class=\"error show\">vollständige E-Mail-Adresse eingeben</div>");
				return 'N';
			} else {
				//inputMsgHide("memberEmail1");
			}
			
			// 이메일 형식 확인
			
			if (!isEmail.test($("#memberEmail1").val()+"@"+$("#"+formId).val()) || isHan.test($("#memberEmail1").val()+"@"+$("#"+formId).val())) {
				inputMsgShow("memberEmail1", "<div class=\"error show\">E-Mail-Format inkorrekt</div>");
				return 'N';
			} else {
				//inputMsgHide("memberEmail1");
			}
			
			// 이메일 중복여부 확인
			
			if (updateValue != $("#memberEmail1").val()+'@'+$("#"+formId).val()) {
				memberAjax("memberEmail");
			} else {
				memberEmailCheck = "Y";
			}
		}
		
		memberEmail2 = "Y";
	}
	
	if (formId == "memberPassword1") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow("memberPassword1", "<div class=\"error show\">Passwort eingeben.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		}
		
		// 입력 여부 확인
		if ($("#"+formId).val().length < 10 || $("#"+formId).val().length > 20 ) {
			inputMsgShow("memberPassword1", "<div class=\"error show\">ein Passwort zwischen 10-20 Zeichen eingeben</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		}

		if ( ($("#"+formId).val().match(isU) != null || $("#"+formId).val().match(isL) != null) &&  $("#"+formId).val().match(isN) != null) {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		} else {
			inputMsgShow("memberPassword1", "<div class=\"error show\">Alphabet and number must be included.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		}
		
		//alert($("#"+formId).val().substring(0,1));
		
		// 3자 이상 동일 문사 사용 여부 확인
		var passwordL = $("#"+formId).val().length;
		var passwordTemp1 = "";
		var passwordTemp2 = "";
		var passwordTemp3 = "";
		var passwordCheck = 0;
		for (var i = 0; i < passwordL; i++) {
			passwordTemp1 = $("#"+formId).val().substring(i,i+1);
			passwordTemp2 = $("#"+formId).val().substring(i+1,i+2);
			passwordTemp3 = $("#"+formId).val().substring(i+2,i+3);
			if (passwordTemp1 == passwordTemp2 && passwordTemp1 == passwordTemp3) {
				passwordCheck++;
			}
		}
		
		if ($("#"+formId).val().indexOf("012") != -1 || $("#"+formId).val().indexOf("123") != -1 
				|| $("#"+formId).val().indexOf("234") != -1 || $("#"+formId).val().indexOf("345") != -1
				|| $("#"+formId).val().indexOf("456") != -1 || $("#"+formId).val().indexOf("567") != -1
				|| $("#"+formId).val().indexOf("678") != -1 || $("#"+formId).val().indexOf("789") != -1) {
			passwordCheck++;
		}
		
		if (passwordCheck > 0) {
			inputMsgShow("memberPassword1", "<div class=\"error show\">Fürs Passwort, gleiche Nummern oder Zeichen können nicht wiederholt verwendet werden.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
			$("#"+formId).addClass("perfect");
			
		}
		memberPassword1 = "Y";
		

		if (updateValue == "Y") {
			if ($("#memberPassword1").val() == "" && $("#memberPassword2").val() == "" ) {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
	}
	
	if (formId == "memberPassword2") {
		if (currentPasswordCheck == "N") {
			// 입력 여부 확인
			if ($("#"+formId).val() == "") {
				inputMsgShow("memberPassword1", "<div class=\"error show\">nochmals Passwort eingeben</div>");
				$("#"+formId).removeClass("perfect");
				return 'N';
			} else {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
			
			// 동일여부 확인
			if ($("#memberPassword1").val() != $("#"+formId).val()) {
				inputMsgShow("memberPassword1", "<div class=\"error show\">Passwort stimmt nicht überein.</div>");
				$("#"+formId).removeClass("perfect");
				return 'N';
			} else {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
		memberPassword2 = "Y";
		
		if (updateValue == "Y") {
			if ($("#memberPassword1").val() == "" && $("#memberPassword2").val() == "" ) {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
	}
	
	if (formId == "companyName1") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyName1", "<div class=\"error show\">Firmenname eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 30) {
			inputMsgShow("companyName1", "<div class=\"error show\">Für den Firmenname, mehr als 30 Zeichen können nicht eingegeben werden.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		companyName = "Y";
	}
	
	if (formId == "companyName2") {
		if ($("#companyName2 option:selected").val() == "") {
			inputMsgShow("companyName1", "<div class=\"error show\">Firmenname wählen.<br /> Wenn Ihr Unternehmen nicht aufgeführt ist, können Sie <a href=\"/de/mobile/qna/qnaList.lg\" class=\"btn inner\">Bewerben Verteiler</a>.</div>");
			return 'N';
		} else {
			//inputMsgHide("companyName1");
			
			//inputMsgHide(formId);
			/*if (updateValue != $("#"+formId).val()) {
				companyAjax();
			} else {
				companyCheck = "Y";
			}*/
			
			
			if (updateValue != $("#companyName2 option:selected").val()) {
				companyAjax();
			} else {
				memberEmailCheck = "Y";
				inputMsgHide("companyName1");
			}
		}
		companyName = "Y";
	}
	
	if (formId == "ceoName") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("ceoName", "<div class=\"error show\">Name des Geschäftsführers eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 30) {
			inputMsgShow("ceoName", "<div class=\"error show\">Für den Geschäftsführername, mehr als 30 Zeichen können nicht eingegeben werden.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		ceoName = "Y";
	}
	
	if (formId == "employeeCount") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("employeeCount", "<div class=\"error show\">Anzahl der Angestellten eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if (isPhone.test($("#"+formId).val()) != "" && isPhone.test($("#"+formId).val()) != true) {
			inputMsgShow("employeeCount", "<div class=\"error show\">nur Zahlen möglich</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
			//memberAjax("companyPhone");
		}
		
		if ($("#"+formId).val().length > 4) {
			inputMsgShow("employeeCount", "<div class=\"error show\">für Anzahl der Angestellten, Eingabe von mehr als 4 Zeichen nicht möglich</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		employeeCount = "Y";
	}
	
	if (formId == "companyPhone") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyPhone", "<div class=\"error show\">Rufnummer eingeben</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
		}
		
		if (isPhone.test($("#"+formId).val()) != "" && isPhone.test($("#"+formId).val()) != true) {
			inputMsgShow("companyPhone", "<div class=\"error show\">nur Zahlen eingeben</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
			if (updateValue != $("#"+formId).val()) {
				memberAjax("companyPhone");
			} else {
				companyPhoneCheck = "Y";
			}
		}
		
		companyPhone = "Y";
	}
	
	if (formId == "companyAddress") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyAddress", "<div class=\"error show\">Addresse eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 100) {
			inputMsgShow("companyAddress", "<div class=\"error show\">bis 100 Zeichen möglich</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		companyAddress = "Y";
	}
	
	if (formId == "companyCity") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyCity", "<div class=\"error show\">Stadt eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 100) {
			inputMsgShow("companyCity", "<div class=\"error show\">bis 100 Zeichen möglich</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		companyCity = "Y";
	}
	
	if (formId == "companyZipcode") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyZipcode", "<div class=\"error show\">PLZ eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if (isPhone.test($("#"+formId).val()) != "" && isPhone.test($("#"+formId).val()) != true) {
			inputMsgShow("companyZipcode", "<div class=\"error show\">nur Zahlen eingeben</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		var url =  document.location.href.split("/");
		if (url[3] == "au") {
			if ($("#"+formId).val().length  != 4) {
				inputMsgShow("companyZipcode", "<div class=\"error show\">Enter the four-digit zip code accurately.</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		} else if (url[3] == "de") {
			if ($("#"+formId).val().length  != 5) {
				inputMsgShow("companyZipcode", "<div class=\"error show\">für die PLZ, 5 Zahlen richtig eingeben</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		} else if (url[3] == "uk") {
			if ($("#"+formId).val().length  >= 5 && $("#"+formId).val().length  <= 7 ) {
				inputMsgShow("companyZipcode", "<div class=\"error show\">Enter the 5~7-digit postal code accurately.</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		}
		
		
		
		companyZipcode = "Y";
	}
	
	if (formId == "preferredDistributor1") {
		if ($("#preferredDistributor1 option:selected").val() == "") {
			inputMsgShow("preferredDistributor1", "<div class=\"error show\">Wählen Sie den Name Ihres bevorzugten Distributors aus</div>");
			$("#preferredDistributorLayer2").hide();
			return 'N';
		} else if ($("#preferredDistributor1 option:selected").val() == "Other") {
			$("#preferredDistributorLayer2").show();
			inputMsgHide(formId);
		}else{
			inputMsgHide(formId);
			$("#preferredDistributorLayer2").hide();
			preferredDistributor2= "Y";
		}
		preferredDistributor1= "Y";
	}
	
	if (formId == "preferredDistributor2") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow("preferredDistributor1", "<div class=\"error show\">Bitte tragen Sie hier den Name Ihres bevorzugten Distributors ein</div>");
			return 'N';
		} else {
			if($("#"+formId).val().length > 30){
				inputMsgShow("preferredDistributor1", "<div class=\"error show\">Enter the 15 .</div>");
				return 'N';
			}else{
				inputMsgHide("preferredDistributor1");
			}
		}
		preferredDistributor2="Y";
	}
}

function businessTypeText(checkValue) {
	if (checkValue == "002") {
		$("#businessTypeMsg").html("<p class=\" list_dot mt_10\"><span class=\"list_head\">&middot;</span>Für Händler ist die Bestätigung des Verwalters nötig und es könnte ca. 1-3 Tage dauern.</p>");
		$("#businessTypeMsg").show();
		$("#companyLayer2").show();
		$("#companyLayer1").hide();
		$("#companyName1Msg").hide();
	} else {
		$("#businessTypeMsg").html("");
		$("#businessTypeMsg").hide();
		$("#companyLayer1").show();
		$("#companyLayer2").hide();
		$("#companyName1Msg").show();
		
	}
}

function formSubmit() {
	
	
	
	
	//alert($('body,html').scrollTop());
	

	
	//alert($("#companyName2 option:selected").val());
	
	memberEmail1 = "N";
	memberEmail2 = "N";
	memberPassword1 = "N";
	memberPassword2 = "N";
	companyName = "N";
	ceoName = "N";
	employeeCount = "N";
	companyPhone = "N";
	companyAddress = "N";
	companyCity = "N";
	companyZipcode = "N";
	
	preferredDistributor1 = "N";
	preferredDistributor2 = "N";
	
	checkForm('memberEmail1');
	checkForm('memberEmail2');
	checkForm('memberPassword1');
	checkForm('memberPassword2');
	
	if ($("input:radio[name='businessType']:checked").val() == "001") { // reseller
		checkForm('companyName1');
		companyCheck = "Y";
	} else { // disty
		checkForm('companyName2');
	}
	
	checkForm('ceoName');
	checkForm('employeeCount');
	checkForm('companyPhone');
	checkForm('companyAddress');
	checkForm('companyCity');
	checkForm('companyZipcode');
	
	checkForm('preferredDistributor1');
	if($("#preferredDistributor1").val() == "Other"){
		checkForm('preferredDistributor2');
	}
	/*alert("memberEmail1 : " + memberEmail1);
	alert("memberEmail2 : " + memberEmail2);
	alert("memberPassword1 : " + memberPassword1);
	alert("memberPassword2 : " + memberPassword2);
	alert("companyName : " + companyName);
	alert("ceoName : " + ceoName);
	alert("employeeCount : " + employeeCount);
	alert("companyPhone : " + companyPhone);
	alert("companyAddress : " + companyAddress);
	alert("companyCity : " + companyCity);
	alert("companyZipcode : " + companyZipcode);
	alert("memberEmailCheck : " + memberEmailCheck);
	alert("companyPhoneCheck : " + companyPhoneCheck);*/
	
	
	if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y"
			&& companyName == "Y" && ceoName == "Y" && employeeCount == "Y" && companyPhone == "Y"
			&& companyAddress == "Y" && companyCity == "Y" && companyZipcode == "Y" && preferredDistributor1 =="Y" && preferredDistributor2 =="Y" && memberEmailCheck == "Y" && companyPhoneCheck == "Y" && companyCheck == "Y") {
		
		
		
		var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
		$("#memberEmail").val(email);
		var password = $("#memberPassword1").val();
		$("#memberPassword").val(password);
		
		if ($("input:radio[name='businessType']:checked").val() == "001") { // reseller
			$("#companyName").val($("#companyName1").val());
		} else {
			$("#companyName").val($("#companyName2 option:selected").val());
		}
		$("#memberForm").submit();
	} else {
		
		// companyCheck
		
		//$("#companyName1").focus();
		
		if (memberEmailCheck == "N") {
			$("#memberEmail2").focus();
		} else if (checkForm('memberEmail1') == "N") {
			$("#memberEmail1").focus();
		} else if (checkForm('memberEmail2') == "N") {
			$("#memberEmail2").focus();
		} else if (checkForm('memberPassword1') == "N") {
			$("#memberPassword1").focus();
		} else if (checkForm('memberPassword2') == "N") {
			$("#memberPassword2").focus();
		} else if ($("input:radio[name='businessType']:checked").val() == "001" && checkForm('companyName1') == "N") {
			$("#companyName1").focus();
		} else if ($("input:radio[name='businessType']:checked").val() == "002" && checkForm('companyName2') == "N") {
			$("#companyName2").focus();
		} else if ($("input:radio[name='businessType']:checked").val() == "002" && companyCheck == "N") {
			$("#companyTitle").focus();
			$('body,html').animate({
				scrollTop: 400
			}, 0);
		} else if (checkForm('ceoName') == "N") {
			$("#ceoName").focus();
		} else if (checkForm('employeeCount') == "N") {
			$("#employeeCount").focus();
		} else if (checkForm('companyPhone') == "N") {
			$("#companyPhone").focus();
		} else if (checkForm('companyAddress') == "N") {
			$("#companyAddress").focus();
		} else if (checkForm('companyCity') == "N") {
			$("#companyCity").focus();
		} else if (checkForm('companyZipcode') == "N") {
			$("#companyZipcode").focus();
		} else if (checkForm('preferredDistributor1') == "N") {
			$("#preferredDistributor1").focus();
		} else if ($("#preferredDistributor1").val() == "Other") {
		    if (checkForm('preferredDistributor2') == "N") {
				$("#preferredDistributor2").focus();
			}
		}
		
		var browser = ieVersion();
		if (Number(browser) > 9) {
			$('body,html').animate({
				scrollTop: $('body,html').scrollTop()-150
			}, 0);
		}
	}
}


function memberAjax(selectWhere) {	
	
	var param = "";
	if (selectWhere == "memberEmail") {
		param = "memberEmail="+$("#memberEmail1").val() + "@" + $("#memberEmail2").val();
	} else if (selectWhere == "companyPhone") {
		param = "companyPhone="+$("#companyPhone").val();
	}
	
	$.ajax({
        type: 'POST',
        url: '/de/front/member/memberFormCheck.lg',
        data : param + '&selectWhere='+selectWhere,
        dataType : 'xml',
        success: function(json){
        	
        	var result = $(json).find("result").text();
        	var selectWhere = $(json).find("selectWhere").text();
        	// 이메일 중복 체크
        	if (selectWhere == "memberEmail") {
        		if (result == 0) {
        			inputMsgHide("memberEmail1");
        			memberEmailCheck = "Y";
        		} else {
        			var altContent = "";
        			altContent += "<div class=\"error show\">Es gibt schon einen anderen Account mit dieser E-Mail-Adresse. Bitte, geben Sie eine andere E-Mail-Adresse ein.";
        			altContent += "	<a href=\"/de/mobile/login/loginForm.lg\" class=\"btn inner\">Einloggen</a>";
        			altContent += "	<a href=\"/de/mobile/member/memberPasswordSearchForm.lg\" class=\"btn inner\">Passwort suchen</a>";
        			altContent += "</div>";
        			inputMsgShow("memberEmail1", altContent);
        			memberEmailCheck = "N";
        		}
        	} else if (selectWhere == "companyPhone") {
        		if (result == 0) {
        			inputMsgHide("companyPhone");
        			companyPhoneCheck = "Y";
        		} else {
        			var memberEmail = $(json).find("memberEmail").text();
        			
        			var altContent = "";
        			altContent+= "<div class=\"error show\">Die Rufnummer wurde schon mit "+memberEmail+" registriert.";
        			altContent+= "	<a href=\"/de/mobile/login/loginForm.lg\" class=\"btn inner\">Einloggen</a>";
        			altContent+= "	<a href=\"/de/mobile/member/memberPasswordSearchForm.lg\" class=\"btn inner\">Passwort suchen</a>";
        			altContent+= "</div>";
        			
        			inputMsgShow("companyPhone", altContent);
        			companyPhoneCheck = "N";
        		}
        	}
        },
        error:function (){
        	alert(e.responseText);
        }
 	});
}

function companyAjax() {	
	
	$.ajax({
        type: 'POST',
        url: '/de/front/member/memberFormCompanyCheck.lg',
        data : 'companyName='+$("#companyName2").val(),
        dataType : 'xml',
        success: function(json){
        	
        	var result = $(json).find("result").text();
        	var companySeq = $(json).find("seq").text();
        	var companyEmail = $(json).find("memberEmail").text();
        	if (companySeq == "") {
        		inputMsgHide("companyName1");
        		companyCheck = "Y";
        	} else {
        		var altContent = "";
    			altContent += "<div class=\"error show\">Ausgewählte Unternehmen sind bereits aufgenommen (id : "+companyEmail+")";
    			altContent += "	<a href=\"/de/mobile/login/loginForm.lg\" class=\"btn inner\">Einloggen</a>";
    			altContent += "	<a href=\"/de/mobile/qna/contactUsUpdateForm.lg\" class=\"btn inner\">Kontakt</a>";
    			altContent += "</div>";
        		inputMsgShow("companyName1", altContent);
        		companyCheck = "N";
        	}
        },
        error:function (){
        	alert(e.responseText);
        }
 	});
}