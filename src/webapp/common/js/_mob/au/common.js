function checkForm() {
	if ($("#loginMemberEmail").val() == "") {		
		alert("Please enter email.");
		$("#loginMemberEmail").focus();
		return;
	}
	if ($("#loginMemberPassword").val() == "") {
		alert("Please enter password.");
		$("#loginMemberPassword").focus();
		return;
	}
	$("#loginForm").submit();
}

function logout() {
	$("#loginForm").attr("action", "/au/mobile/login/logout.lg");
	$("#loginForm").submit();
	
	//location.href = "/au/mobile/login/logout.lg";
}

function loginSubmit() {	
	
	if ($("#loginMemberEmail").val() == "") {		
		alert("Please enter email.");
		$("#loginMemberEmail").focus();
		return;
	}
	
	if ($("#loginMemberPassword").val() == "") {
		alert("Please enter password.");
		$("#loginMemberPassword").focus();
		return;
	}
	
	var loginTrue = "";
	if ($("#loginTrue").is(":checked")) {
		loginTrue = "Y";
	}
	
	$.ajax({
        type: 'POST',  //포스트방식
        url: sslDomain+'/au/mobile/login/login.lg',     //요청처리
        data : 'memberEmail='+$("#loginMemberEmail").val() + "&memberPassword="+$("#loginMemberPassword").val()+"&loginTrue="+loginTrue, //파라미터
        dataType : 'jsonp',      //처리한 요청을 받는 형식
        jsonp : 'getLogin',      //처리한 요청을 받는 형식
        success: function(json){       //성공시 해당함수 실행
        	
        	
        },
        error:function (e){
        	//alert(e.responseText);
        }
 	});
}

function getLogin(jsonpData) {
	emailSave($("#loginMemberEmail").val());
	
	
	
	var result = jsonpData.loginResultCode;
	
	if (result == "001") { // 로그인성공
		
		//if ($("#loginTrue").is(":checked")) {
    	//	setCookie('essb2blogin', $(json).find("string").eq(1).text(), 100);
    	//} else {
    	//	setCookie('essb2blogin', $(json).find("string").eq(1).text(), -1);
    	//}
		
		document.location.hash = "#confirm=Y";
		location.href = "/au/mobile/main/main.lg";
	} else if (result == "101") { // 로그인실패(이메일과 비밀번호가 정확하지 않습니다.)
		alert("Email and password are not correct.");
	} else if (result == "102") { //로그인실패(이메일을 입력해주세요.)
		alert("Please enter email.");
	} else if (result == "103") { // 로그인실패(비밀번호를 입력해주세요.)
		alert("Please enter password.");
	} else if (result == "201") { // 로그인실패(관리자 승인 대기 중입니다.)
		location.href = essDomain+"/au/mobile/login/loginDistyComplete.lg?memberEmail="+$("#loginMemberEmail").val();
	} else if (result == "202") { // 로그인실패(관리자 승인 거부)
		// 해당페이지 이동
		location.href = essDomain+"/au/mobile/login/loginDistyNotComplete.lg?memberEmail="+$("#loginMemberEmail").val();
	} else if (result == "301") { // 90일이상 장기 미접속차 처리
		// 해당페이지 이동
		location.href = essDomain+"/au/mobile/member/memberLoginLockForm.lg";
	}
}

function emailSave(email) {
	
	if ($("#emailSaveCheck").is(":checked")) {
		setCookie('essb2bcookie', email, 100);
	} else {
		setCookie('essb2bcookie', email, -1);
	}
}

function setCookie(cName, cValue, cDay){
    var expire = new Date();
    expire.setDate(expire.getDate() + cDay);
    cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
    if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
    document.cookie = cookies;
}

// 쿠키 가져오기
function getCookie(cName) {
    cName = cName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cName);
    var cValue = '';
    if(start != -1){
        start += cName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cValue = cookieData.substring(start, end);
    }
    return unescape(cValue);
}

function getFileIcon(fileName) {
	var fileExt = fileName.slice(fileName.indexOf(".")+1).toLowerCase();
	var result = "";
	if (fileExt == "pdf") {
		result = "pdf";
	} else if (fileExt == "xls" || fileExt == "xlsx") {
		result = "xls";
	} else if (fileExt == "jpg" || fileExt == "png" || fileExt == "gif" || fileExt == "bmp") {
		if (fileExt == "jpg") {
			result = "jpg";
		} else if (fileExt == "gif") {
			result = "gif";
		}
	} else if (fileExt == "zip") {
		result = "zip";
	} else if (fileExt == "doc" || fileExt == "docx") {
		result = "doc";	
	}
	return result;
}

function pubByteCheckTextarea(oid,tid){
	$(oid).on("keyup",function(){
		var byteTxt = "";
		var byte = function(str){
			var byteNum=0;
			for(i=0;i<str.length;i++){
				byteNum+=(str.charCodeAt(i)>127)?2:1;
				if(byteNum<500){
					byteTxt+=str.charAt(i);
				};
			};
			return Math.round( byteNum );
		};
		if(byte($(this).val())>2000){
			alert("Enter up to 2,000 bytes.");
			$(this).val("");
			$(this).val(byteTxt);
		}else{
			$(tid).html( byte($(this).val()) )
		}
	});
};

$(document).ready(function(){
	
	if (getCookie("essb2bcookie") != "") {
		$("#emailSaveCheck").attr("checked", "checked");
		$("#loginMemberEmail").val(getCookie("essb2bcookie"));
	}
});

function ieVersion () { 
	 var word; 
	 var version = "N/A"; 
	 var agent = navigator.userAgent.toLowerCase(); 
	 var name = navigator.appName; 
	 // IE old version ( IE 10 or Lower ) 
	 if ( name == "Microsoft Internet Explorer" ) word = "msie "; 
	 else { 
		 // IE 11 
		 if ( agent.search("trident") > -1 ) word = "trident/.*rv:"; 
		 // Microsoft Edge  
		 else if ( agent.search("edge/") > -1 ) word = "edge/"; 
	 } 
	 var reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 
	 if (  reg.exec( agent ) != null  ) version = RegExp.$1 + RegExp.$2; 
	 return version; 
} 


function imgLoad(imgSrc, imgId) {
	$("#"+imgId).on('load', function() {
	    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
	    	
	      } else {
	    	  $("#"+imgId).attr("src", imgSrc);
	      }
	  });
}
