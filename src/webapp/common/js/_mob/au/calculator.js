// Input Value 숫자, 소수점 체크
function inputChk(inputId) {
	var isNumber = /[^0-9.]/g;

	if (isNumber.test($("#"+inputId).val())) {
		$("#"+inputId).val("");
		$("#"+inputId).attr("placeholder", "Only numbers can be entered");
		return;
	}
}

// 천단위 콤마
function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var selType = 1;  // Solar plus battery 초기값
function formInit() {  //초기화
	$(':text:not([id=cal])').val('');
	$('input[type=number]').val('');
	$("#cal17").val("0.11");
	
	// 결과값 영역 초기화
	$("#item3_subsidy_val_css").removeClass("none");
	$("#item3_subsidy_css").attr("class", "none");
	$("#item3_subsidy_result").html("");
	
	$("#item3_withoutsubsidy_val_css").removeClass("none");
	$("#item3_withoutsubsidy_css").attr("class", "none");
	$("#item3_withoutsubsidy_result").html("");
	
	$("#item3_withsubsidy_val_css").removeClass("none");
	$("#item3_withsubsidy_css").attr("class", "none");
	$("#item3_withsubsidy_result").html("");
	$("#item3_withsubsidy_result_hidden").val("");
	
	$("#item4_pvgenerated_val_css").removeClass("none");
	$("#item4_pvgenerated_css").attr("class", "none");
	$("#item4_pvgenerated_result").html("");
	$("#item4_pvgenerated_result_hidden").val("");
	
	$("#item5_averagepvself_val_css").removeClass("none");
	$("#item5_averagepvself_css").attr("class", "none");
	$("#item5_averagepvself_result").html("");
	$("#item5_averagepvself_result_hidden").val("");
	
	//20160526 추가
	$("#item5_averagebatteryself_val_css").removeClass("none");
	$("#item5_averagebatteryself_css").attr("class", "none");
	$("#item5_averagebatteryself_result").html("");
	//20160526 추가
	$("#item5_averagefitself_val_css").removeClass("none");
	$("#item5_averagefitself_css").attr("class", "none");
	$("#item5_averagefitself_result").html("");
	
	$("#item6_averageessself_val_css").removeClass("none");
	$("#item6_averageessself_css").attr("class", "none");
	$("#item6_averageessself_result").html("");
	$("#item6_averageessself_result_hidden").val("");
	$("#item7_averageenergyexport_result_hidden").val("");
	
	$("#item8_revenuepvself_val_css").removeClass("none");
	$("#item8_revenuepvself_css").attr("class", "none");
	$("#item8_revenuepvself_result").html("");
	$("#item8_revenuepvself_result_hidden").val("");
	//20160526
	$("#item8_revenuebatteryself_val_css").removeClass("none");
	$("#item8_revenuebatteryself_css").attr("class", "none");
	$("#item8_revenuebatteryself_result").html("");
	
	$("#item9_revenueessself_val_css").removeClass("none");
	$("#item9_revenueessself_css").attr("class", "none");
	$("#item9_revenueessself_result").html("");
	
	$("#item10_revenueenergyexport_val_css").removeClass("none");
	$("#item10_revenueenergyexport_css").attr("class", "none");
	$("#item10_revenueenergyexport_result").html("");
	$("#item10_revenueenergyexport_result_hidden").val("");
	
	$("#item11_decreaseenergyexport_val_css").removeClass("none");
	$("#item11_decreaseenergyexport_css").attr("class", "none");
	$("#item11_decreaseenergyexport_result").html("");
	
	$("#item2_data").html("");
	$("#item3_data").html("");
	$("#item4_data").html("");
	$("#item5_data").html("");
	$("#item6_data").html("");
	$("#item7_data").html("");
	$("#item8_data").html("");
	$("#item9_data").html("");
	$("#item10_data").html("");
	$("#item11_data").html("");
	$("#item12_data").html("Please enter all of the above items");
	$("#item13_data").html("Please enter all of the above items");
	$("#item14_data").html("Please enter all of the above items");

	
	// 입력창 모두 열기
	$(".acc_tit").addClass("on");
	$(".acc_cont").removeClass("blind");
	// 입력창 모두 닫기
	//$('.acc_box').find('.acc_tit').removeClass('on');
	//$('.acc_box').find('.acc_cont').addClass('blind');	
}		

function selTypeFnc() {	
	var idx = $("#installType option:selected").val();
	var item1_txt = "";
	if (idx == 1) {
		item1_txt = "Solar plus battery";
		$("#pvPanel").show();
		$("#essBattery").show();
		$("#pvSystemCap").show();
		$("#essBatteryCap").show();
		$("#revenue_ess_self").show();
		$("#pv_generated_power").show();
		
		$("#average_energy_export").show();
		$("#revenue_pv_self").show();
		$("#revenue_energy_export").show();				
		$("#average_pv_self").show();		
		
		//$("#item6_list").hide();
		$("#average_ess_self").hide();
		$("#average_energy_export").hide();
		//$("#item9_list").hide();
		$("#revenue_ess_self").hide();
		$("#decrease_energy_export").hide();
		
		$("#average_ess_self_num").html("6. ");
		$("#revenue_ess_self_num").html("9. ");
		$("#decrease_energy_export_num").html("11. ");
		
		$("#average_ess_self .acc_tit").addClass("bg_no");
		$("#revenue_ess_self .acc_tit").addClass("bg_no");
		
		selType = 1;
	} else if (idx == 2) {
		item1_txt = "Battery only";
		$("#pvPanel").hide();
		$("#pvSystemCap").hide();
		$("#pv_generated_power").hide();	
		$("#average_energy_export").hide();
		$("#revenue_pv_self").hide();
		$("#revenue_energy_export").hide();				
		$("#average_pv_self").hide();

		
		$("#revenue_ess_self").show();
		$("#essBattery").show();
		$("#essBatteryCap").show();
		//$("#item6_list").show();
		$("#average_ess_self").show();
		$("#item9_list").show();
		$("#decrease_energy_export").show();
		
		$("#average_ess_self_num").html("4. ");
		$("#revenue_ess_self_num").html("5. ");
		$("#decrease_energy_export_num").html("6. ");
		
		$("#average_ess_self .acc_tit").removeClass("bg_no");
		$("#revenue_ess_self .acc_tit").removeClass("bg_no");
		
		$("#item6_data").removeAttr("style");
		$("#item9_data").removeAttr("style");
		
		selType = 2;
	}
	
	$("#item1_data").html(item1_txt);
	formInit();  // 폼 초기화
	
	$("#cal9").val("2");
	//$("#cal9").attr("readonly",true);
	$("#cal9").attr("placeholder", "");
	$("#cal10").val("90");
	//$("#cal10").attr("readonly",true);
	$("#cal10").attr("placeholder", "");
	$("#cal15").val("2");
	//$("#cal15").attr("readonly",true);
	$("#cal15").attr("placeholder", "");
	$("#cal16").val("90");
	//$("#cal16").attr("readonly",true);
	$("#cal16").attr("placeholder", "");
}

// 항목 값 체크 후 계산
function checkVal(item_idx) {

	// Total Power Consumption per Year of the house :::::::::::::::::::::::::::::
	if (item_idx == 2) {				
		if ($("#cal1").val() != "" && $("#cal1").val() != null) {
			$("#item2_data").html(numberWithCommas($("#cal1").val()) + " kWh");
		} else {
		}
	}

	// ** 아래 재계산 부분은 중간 필수값을 변경시 재계산 필요 **
	// Total System Cost :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	var item3_cnt = 0;
	var input3_cnt = 0;
	var item3_withoutsubsidy_result = 0;
	if (item_idx == 3) {
		$("#item3_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item3_cnt++;
			}
		});
		$("#item3_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input3_cnt++;
			}
		});

		if (item3_cnt == input3_cnt) {
			if (selType == 1) {
				item3_withoutsubsidy_result = (parseFloat($("#cal2").val()) + parseFloat($("#cal3").val()) + parseFloat($("#cal4").val()) + parseFloat($("#cal5").val())) * (1 + (parseFloat($("#cal6").val())/100));
			} else if (selType == 2) {
				item3_withoutsubsidy_result = (parseFloat($("#cal3").val()) + parseFloat($("#cal4").val()) + parseFloat($("#cal5").val())) * (1 + (parseFloat($("#cal6").val())/100));
			}

			$("#item3_withoutsubsidy_result").html(numberWithCommas(item3_withoutsubsidy_result));
			$("#item3_withoutsubsidy_val_css").addClass("none");
			$("#item3_withoutsubsidy_css").attr("class", "block");
			
			$("#item3_withsubsidy_result").html(numberWithCommas(item3_withoutsubsidy_result - (parseFloat($("#cal27").val()))));					
			$("#item3_withsubsidy_val_css").addClass("none");
			$("#item3_withsubsidy_css").attr("class", "block");
			
			$("#item3_withsubsidy_result_hidden").val(item3_withoutsubsidy_result - (parseFloat($("#cal27").val())));
			
			$("#item3_data").html(numberWithCommas(item3_withoutsubsidy_result - (parseFloat($("#cal27").val()))) + " AUD");
			
			if (selType == 1) {
				checkVal(10);  // Revenue by Energy Export (FiT) per Year 재계산
			} else {
				checkVal(11);  // Decrease of Energy Export (FiT) Revenue 재계산
			}
		} else {					
			$("#item3_subsidy_result").html("");
			$("#item3_subsidy_val_css").removeClass("none");
			$("#item3_subsidy_css").attr("class", "none");
			
			$("#item3_withsubsidy_result").html("");
			$("#item3_withsubsidy_val_css").removeClass("none");
			$("#item3_withsubsidy_css").attr("class", "none");
			
			$("#item3_withoutsubsidy_result").html("");
			$("#item3_withoutsubsidy_val_css").removeClass("none");
			$("#item3_withoutsubsidy_css").attr("class", "none");
			
			$("#item3_withsubsidy_result_hidden").val("");
			
			$("#item3_data").html("");
			$("#item11_data").html("");
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}
	}
	
	// PV Generated Power per Year :::::::::::::::::::::::::::::::::::::::::::::::
	var item4_cnt = 0;
	var input4_cnt = 0;
	var item4_pvgenerated_result = 0;
	if (item_idx == 4) {
		$("#item4_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item4_cnt++;
			}
		});
		$("#item4_pvgenerated_result_hidden").val("");
		$("#item4_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input4_cnt++;
			}
		});			

		if (item4_cnt == input4_cnt) {
			if (selType == 1) {
				item4_pvgenerated_result = parseFloat($("#cal7").val()) * parseFloat($("#cal11").val()) * 24 * 365;
			}
			$("#item4_pvgenerated_result").html(numberWithCommas(Math.round(item4_pvgenerated_result * 1) / 1));
			$("#item4_pvgenerated_val_css").addClass("none");
			$("#item4_pvgenerated_css").attr("class", "block");
			
			$("#item4_pvgenerated_result_hidden").val((Math.round(item4_pvgenerated_result * 1) / 1));
			
			$("#item4_data").html(numberWithCommas(Math.round(item4_pvgenerated_result * 1) / 1) + " kWh");
			
			
		} else {
			$("#item4_pvgenerated_val_css").removeClass("none");
			$("#item4_pvgenerated_css").attr("class", "none");
			$("#item4_pvgenerated_result").html("");
			
			$("#item4_pvgenerated_result_hidden").val("");
			
			$("#item4_data").html("");
			
			// Average PV Self-Consumption per Year 초기화
			$("#item5_averagepvself_val_css").removeClass("none");
			$("#item5_averagepvself_css").attr("class", "none");
			$("#item5_averagepvself_result").html("");
			// 20160526 battery 초기화
			$("#item5_averagebatteryself_val_css").removeClass("none");
			$("#item5_averagebatteryself_css").attr("class", "none");
			$("#item5_averagebatteryself_result").html("");
			// 20160526 fit 초기화
			$("#item5_averagefitself_val_css").removeClass("none");
			$("#item5_averagefitself_css").attr("class", "none");
			$("#item5_averagefitself_result").html("");
			
			$("#item5_averagepvself_result_hidden").val("");
			$("#item6_averageessself_result_hidden").val("");
			$("#item7_averageenergyexport_result_hidden").val("");
			$("#item10_revenueenergyexport_result_hidden").val("");
			
			$("#item5_data").html("");
			$("#item6_data").html("");
			$("#item7_data").html("");
			
			// Revenue by PV Self-Consumption per Year 초기화
			$("#item8_revenuepvself_val_css").removeClass("none");
			$("#item8_revenuepvself_css").attr("class", "none");
			$("#item8_revenuepvself_result").html("");
			$("#item8_revenuepvself_result_hidden").val("");
			
			// 20160526 Revenue by battery Self-Consumption per Year 초기화
			$("#item8_revenuebatteryself_val_css").removeClass("none");
			$("#item8_revenuebatteryself_css").attr("class", "none");
			$("#item8_revenuebatteryself_result").html("");
			
			$("#item8_data").html("");
			$("#item9_data").html("");
			
			// Revenue by Energy Export (FiT) per Year 초기화
			$("#item10_revenueenergyexport_val_css").removeClass("none");
			$("#item10_revenueenergyexport_css").attr("class", "none");
			$("#item10_revenueenergyexport_result").html("");
			
			$("#item10_data").html("");
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}
		
		checkVal(5);  // Average PV Self-Consumption per Year 재계산
		if ($("#item6_averageessself_result_hidden").val() != "") {
			checkVal(8);  // Revenue by PV Self-Consumption per Year 재계산
		}
		if ($("#item7_averageenergyexport_result_hidden").val() != "") {
			checkVal(10);  // Revenue by Energy Export (FiT) per Year 재계산
		}
	}
	// Average PV Self-Consumption per Year ::::::::::::::::::::::::::::::::::::::
	var item5_cnt = 0;
	var input5_cnt = 0;
	var item5_averagepvself_result = 0;
	var average_ess_self_result = 0;  // Average ESS Self-Consumption per Year 결과값
	var average_energy_export_result = 0;  //Average Energy Export (FiT) on Grid per Year 결과값
	if (item_idx == 5) {
		$("#item5_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item5_cnt++;
			}
		});
		$("#item5_averagepvself_result_hidden").val("");
		$("#item6_averageessself_result_hidden").val("");
		$("#item5_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input5_cnt++;
			}
		});

		if (item5_cnt == input5_cnt && $("#item4_pvgenerated_result_hidden").val() != "") {
			if (selType == 1) {
				item5_averagepvself_result = parseFloat($("#item4_pvgenerated_result_hidden").val()) * (parseFloat($("#cal12").val())/100);
			}

			$("#item5_averagepvself_result").html(numberWithCommas(Math.round(item5_averagepvself_result * 1) / 1));
			$("#item5_averagepvself_val_css").addClass("none");
			$("#item5_averagepvself_css").attr("class", "block");
			
			$("#item5_averagepvself_result_hidden").val(Math.round(item5_averagepvself_result * 1) / 1);
			
			$("#item5_data").html(numberWithCommas(Math.round(item5_averagepvself_result * 1) / 1) + " kWh");
			
			for (var t = 0; t < 10 ; t++ )
			{
				average_ess_self_result = average_ess_self_result + ( parseFloat($("#cal8").val()) * (parseFloat($("#cal10").val())/100) * parseFloat($("#cal13").val()) * (1- ( (parseFloat($("#cal9").val())/100) * t) ) );						
			}
			//20160526
			$("#item5_averagebatteryself_result").html(numberWithCommas(Math.round(average_ess_self_result/10 * 1) / 1));
			$("#item5_averagebatteryself_val_css").addClass("none");
			$("#item5_averagebatteryself_css").attr("class", "block");
			
			$("#item6_data").html(numberWithCommas(Math.round((average_ess_self_result/10) * 1) / 1) + " kWh");
			$("#item6_data").show();
			
			$("#item6_averageessself_result_hidden").val(Math.round((average_ess_self_result/10) * 1) / 1);
			
			if ($("#item4_pvgenerated_result_hidden").val() != "" && $("#item5_averagepvself_result_hidden").val() != "" && $("#item6_averageessself_result_hidden").val() != "") {
				average_energy_export_result = ( $("#item4_pvgenerated_result_hidden").val() - $("#item5_averagepvself_result_hidden").val() - $("#item6_averageessself_result_hidden").val() );
				
				//20160526
				$("#item5_averagefitself_result").html(numberWithCommas(Math.round((average_energy_export_result) * 1) / 1));
				$("#item5_averagefitself_val_css").addClass("none");
				$("#item5_averagefitself_css").attr("class", "block");
				
				$("#item7_data").html(numberWithCommas(Math.round((average_energy_export_result) * 1) / 1) + " kWh");
				$("#item7_data").show();
				
				$("#item7_averageenergyexport_result_hidden").val(Math.round((average_energy_export_result) * 1) / 1);
				
				if ($("#item6_averageessself_result_hidden").val() != "") {
					checkVal(8);  // Revenue by PV Self-Consumption per Year 재계산
				}
				if ($("#item7_averageenergyexport_result_hidden").val() != "") {
					checkVal(10);  // Revenue by Energy Export (FiT) per Year 재계산
				}
			}
		} else {
			$("#item5_averagepvself_val_css").removeClass("none");
			$("#item5_averagepvself_css").attr("class", "none");
			$("#item5_averagepvself_result").html("");
			
			$("#item10_revenueenergyexport_val_css").removeClass("none");
			$("#item10_revenueenergyexport_css").attr("class", "none");
			$("#item10_revenueenergyexport_result").html("");
								
			$("#item5_averagepvself_result_hidden").val("");
			$("#item6_averageessself_result_hidden").val("");
			$("#item7_averageenergyexport_result_hidden").val("");
			$("#item10_revenueenergyexport_result_hidden").val("");					
			
			$("#item5_data").html("");
			$("#item6_data").html("");
			$("#item6_data").hide();
			$("#item7_data").html("");
			$("#item7_data").hide();
			$("#item9_data").html("");
			$("#item9_data").hide();
			$("#item10_data").html("");
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}
	}
	
	// Average ESS Self-Consumption per Year(Battery only) :::::::::::::::::::::::::::
	var item6_cnt = 0;
	var input6_cnt = 0;
	var item6_averageessself_result = 0;
	if (item_idx == 6) {
		$("#item6_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item6_cnt++;
			}
		});
		$("#item6_averageessself_result_hidden").val("");
		$("#item6_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input6_cnt++;
			}
		});

		if (item6_cnt == input6_cnt) {
			if (selType == 2) {
				for (var t = 0; t < 10 ; t++ )
				{
					item6_averageessself_result = item6_averageessself_result + ( parseFloat($("#cal14").val()) * (parseFloat($("#cal16").val())/100) * parseFloat($("#cal18").val()) * (1- ( (parseFloat($("#cal15").val())/100) * t) ) );						
				}	
			}
			$("#item6_averageessself_result").html(numberWithCommas(Math.round((item6_averageessself_result/10) * 1) / 1));
			$("#item6_averageessself_val_css").addClass("none");
			$("#item6_averageessself_css").attr("class", "block");
			
			$("#item6_averageessself_result_hidden").val(Math.round((item6_averageessself_result/10) * 1) / 1);

			$("#item6_data").html(numberWithCommas(Math.round((item6_averageessself_result/10) * 1) / 1) + " kWh");
		} else {
			$("#item6_averageessself_val_css").removeClass("none");
			$("#item6_averageessself_css").attr("class", "none");
			$("#item6_averageessself_result").html("");
			
			$("#item6_averageessself_result_hidden").val("");
			
			$("#item6_data").html("");
		}
		
		checkVal(9);  // Revenue by ESS Self-Consumption per Year 재계산
		if (selType == 2) {
			checkVal(11);  // Decrease of Energy Export (FiT) Revenue 재계산
		}
	}
	
	// Revenue by PV Self-Consumption per Year  ::::::::::::::::::::::::::::::::::
	var item8_cnt = 0;
	var input8_cnt = 0;
	var item8_revenuepvself_result = 0;  // Revenue by PV Self-Consumption per Year
	var revenue_ess_self_result = 0;  // Revenue by ESS Self-Consumption per Year
	if (item_idx == 8) {
		$("#item8_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item8_cnt++;
			}
		});

		$("#item8_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input8_cnt++;
			}
		});
		
		if (item8_cnt == input8_cnt) {
			if (selType == 1) {
				for (var t = 0; t < 10 ; t++ )
				{
					item8_revenuepvself_result = item8_revenuepvself_result + (parseFloat($("#item5_averagepvself_result_hidden").val()) * parseFloat($("#cal19").val()) * (Math.pow( 1 + (parseFloat($("#cal20").val())/100), t )));
					if ($("#item6_averageessself_result_hidden").val() != "") {
					revenue_ess_self_result = revenue_ess_self_result + (parseFloat($("#item6_averageessself_result_hidden").val()) * parseFloat($("#cal19").val()) * (Math.pow( 1 + (parseFloat($("#cal20").val())/100), t )));
					}
				}
			}
			
			$("#item8_revenuepvself_result").html(numberWithCommas(Math.round((item8_revenuepvself_result/10) * 1) / 1));
			$("#item8_revenuepvself_val_css").addClass("none");
			$("#item8_revenuepvself_css").attr("class", "block");
			
			$("#item8_revenuepvself_result_hidden").val(Math.round((item8_revenuepvself_result/10) * 1) / 1);
			
			$("#item8_data").html(numberWithCommas(Math.round((item8_revenuepvself_result/10) * 1) / 1) + " AUD");
			
			if ($("#item6_averageessself_result_hidden").val() != "") {
				$("#item8_revenuebatteryself_result").html(numberWithCommas(Math.round((revenue_ess_self_result/10) * 1) / 1));
				$("#item8_revenuebatteryself_val_css").addClass("none");
				$("#item8_revenuebatteryself_css").attr("class", "block");
				
				
				$("#revenue_ess_self_result").val(Math.round((revenue_ess_self_result/10) * 1) / 1);
				$("#item9_data").html(numberWithCommas(Math.round((revenue_ess_self_result/10) * 1) / 1) + " AUD");
				$("#item9_data").show();
			}
		} else {
			$("#item8_revenuepvself_val_css").removeClass("none");
			$("#item8_revenuepvself_css").attr("class", "none");
			$("#item8_revenuepvself_result").html("");
			$("#item8_revenuepvself_result_hidden").val("");
			
			$("#item8_revenuebatteryself_val_css").removeClass("none");
			$("#item8_revenuebatteryself_css").attr("class", "none");
			$("#item8_revenuebatteryself_result").html("");
			
			$("#item8_data").html("");
			$("#item9_data").html("");
			$("#item9_data").hide();
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}
		
		checkVal(10);  // Revenue by Energy Export (FiT) per Year 재계산
	}
	
	// Revenue by ESS Self-Consumption per Year  :::::::::::::::::::::::::::::::::
	var item9_cnt = 0;
	var input9_cnt = 0;
	var item9_revenueessself_result = 0;
	if (item_idx == 9) {
		$("#item9_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item9_cnt++;
			}
		});

		$("#item9_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input9_cnt++;
			}
		});

		if (item9_cnt == input9_cnt && $("#item6_averageessself_result_hidden").val() != "") {
			if (selType == 2) {
				for (var t = 0; t < 10 ; t++ )
				{
					item9_revenueessself_result = item9_revenueessself_result + (parseFloat($("#item6_averageessself_result_hidden").val()) * parseFloat($("#cal21").val()) * (Math.pow( 1 + (parseFloat($("#cal22").val())/100), t )));
				}
			}
			$("#item9_revenueessself_result").html(numberWithCommas(Math.round((item9_revenueessself_result/10) * 1) / 1));
			$("#item9_revenueessself_val_css").addClass("none");
			$("#item9_revenueessself_css").attr("class", "block");
			
			$("#revenue_ess_self_result").val(Math.round((item9_revenueessself_result/10) * 1) / 1);
			
			$("#item9_data").html(numberWithCommas(Math.round((item9_revenueessself_result/10) * 1) / 1) + " AUD");
			if (selType == 1) {
			$("#item9_data").show();
			}
		} else {
			$("#item9_revenueessself_val_css").removeClass("none");
			$("#item9_revenueessself_css").attr("class", "none");
			$("#item9_revenueessself_result").html("");
			
			$("#revenue_ess_self_result").val("");
			
			$("#item9_data").html("");
			if (selType == 1) {
			$("#item9_data").hide();
			}
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}
		
		if (selType == 2) {
			checkVal(11);  // Decrease of Energy Export (FiT) Revenue 재계산
		}				
	}
	
	// Revenue by Energy Export (FiT) per Year  ::::::::::::::::::::::::::::::::::
	var item10_cnt = 0;
	var input10_cnt = 0;
	var item10_revenueenergyexport_result = 0;  // Revenue by Energy Export (FiT) per Year
	if (item_idx == 10) {
		$("#item10_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item10_cnt++;
			}
		});

		$("#item10_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input10_cnt++;
			}
		});
		
		if (item10_cnt == input10_cnt) {
			if (selType == 1) {
				for (var t = 0; t < 10 ; t++ )
				{
					item10_revenueenergyexport_result = item10_revenueenergyexport_result + (parseFloat($("#item7_averageenergyexport_result_hidden").val()) * parseFloat($("#cal23").val()) * (Math.pow( 1 - (parseFloat($("#cal24").val())/100), t )));
				}
			}					
			$("#item10_revenueenergyexport_result").html(numberWithCommas(Math.round((item10_revenueenergyexport_result/10) * 1) / 1));
			$("#item10_revenueenergyexport_val_css").addClass("none");
			$("#item10_revenueenergyexport_css").attr("class", "block");
			
			$("#item10_revenueenergyexport_result_hidden").val(Math.round((item10_revenueenergyexport_result/10) * 1) / 1);
			
			$("#item10_data").html(numberWithCommas(Math.round((item10_revenueenergyexport_result/10) * 1) / 1) + " AUD");
			
			// ROI Calculation Result 값설정
			if ($("#item3_withsubsidy_result_hidden").val() != "" & $("#item8_revenuepvself_result_hidden").val() != "" && $("#revenue_ess_self_result").val() != "" && $("#item10_revenueenergyexport_result_hidden").val() != "") {
				// Average Annual Profit [Local Dollar]
				$("#item12_data").show();			
				$("#item12_data").html(numberWithCommas(parseFloat($("#item8_revenuepvself_result_hidden").val()) + parseFloat($("#revenue_ess_self_result").val()) + parseFloat($("#item10_revenueenergyexport_result_hidden").val())) + " AUD");
				
				// Average Annual Profit %
				$("#item13_data").show();
				$("#item13_data").html(Math.round( (((parseFloat($("#item8_revenuepvself_result_hidden").val()) + parseFloat($("#revenue_ess_self_result").val()) + parseFloat($("#item10_revenueenergyexport_result_hidden").val())) / parseFloat($("#item3_withsubsidy_result_hidden").val())) * 100)) + " %");
				
				// ROI Years [Years]
				$("#item14_data").show();
				$("#item14_data").html(Math.round( ((parseFloat($("#item3_withsubsidy_result_hidden").val()) / (parseFloat($("#item8_revenuepvself_result_hidden").val()) + parseFloat($("#revenue_ess_self_result").val()) + parseFloat($("#item10_revenueenergyexport_result_hidden").val())) )) * 10) / 10);
			}
		} else {
			$("#item10_revenueenergyexport_val_css").removeClass("none");
			$("#item10_revenueenergyexport_css").attr("class", "none");
			$("#item10_revenueenergyexport_result").html("");

			$("#item10_data").html("");
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}				
	}
	
	// Decrease of Energy Export (FiT) Revenue  ::::::::::::::::::::::::::::::::::
	var item11_cnt = 0;
	var input11_cnt = 0;
	var item11_decreaseenergyexport_result = 0;
	if (item_idx == 11) {
		$("#item11_list input").each(function(index) {	// input 개수 체크			
			if ($(this).parent().css("display") == "block") {
				item11_cnt++;
			}
		});

		$("#item11_list input").each(function(index) {  // input 값 체크
			if ($(this).val() != "Only numbers" && $(this).val() != "Only numbers or point" && $(this).val() != "" && ($(this).attr("type") == "text" || $(this).attr("type") == "number")) {
				input11_cnt++;
			}
		});

		if (item11_cnt == input11_cnt && $("#item6_averageessself_result_hidden").val() != "") {
			if (selType == 2) {
				for (var t = 0; t < 10 ; t++ )
				{
					item11_decreaseenergyexport_result = item11_decreaseenergyexport_result + (parseFloat($("#item6_averageessself_result_hidden").val()) * parseFloat($("#cal25").val()) * (Math.pow( 1 + (parseFloat($("#cal26").val())/100), t )));
				}
			}
			$("#item11_decreaseenergyexport_result").html(numberWithCommas(Math.round((item11_decreaseenergyexport_result/10) * 1) / 1));
			$("#item11_decreaseenergyexport_val_css").addClass("none");
			$("#item11_decreaseenergyexport_css").attr("class", "block");
			
			$("#decrease_energy_export_result").val(Math.round((item11_decreaseenergyexport_result/10) * 1) / 1);
			
			$("#item11_data").html(numberWithCommas(Math.round((item11_decreaseenergyexport_result/10) * 1) / 1) + " AUD");
			
			// ROI Calculation Result 값설정
			if ($("#item3_withsubsidy_result_hidden").val() != "" & $("#item6_averageessself_result_hidden").val() != "" && $("#revenue_ess_self_result").val() != "" && $("#decrease_energy_export_result").val() != "") {
				// Average Annual Profit [Local Dollar]
				$("#item12_data").show();			
				$("#item12_data").html(numberWithCommas(parseFloat($("#revenue_ess_self_result").val()) - parseFloat($("#decrease_energy_export_result").val()) + " AUD"));
				
				// Average Annual Profit %
				$("#item13_data").show();
				$("#item13_data").html(Math.round( (((parseFloat($("#revenue_ess_self_result").val()) - parseFloat($("#decrease_energy_export_result").val())) / parseFloat($("#item3_withsubsidy_result_hidden").val())) * 100)) + " %");
				
				// ROI Years [Years]
				$("#item14_data").show();
				$("#item14_data").html(Math.round( ((parseFloat($("#item3_withsubsidy_result_hidden").val()) / (parseFloat($("#revenue_ess_self_result").val()) - parseFloat($("#decrease_energy_export_result").val())) )) * 10) / 10);
			}
		} else {
			$("#item11_decreaseenergyexport_val_css").removeClass("none");
			$("#item11_decreaseenergyexport_css").attr("class", "none");
			$("#item11_decreaseenergyexport_result").html("");
			
			$("#decrease_energy_export_result").val("");
			
			$("#item11_data").html("");
			$("#item12_data").html("Please enter all of the above items");
			$("#item13_data").html("Please enter all of the above items");
			$("#item14_data").html("Please enter all of the above items");
		}		
	}	
}