// Window Resize
$(window).bind("resize", function(){
	
	// full layout
	full_height();

});


$(window).on("scroll touchmove", function () {
	
});

$(document).ready(function(){
	
	/*
		layout
	----------------------------------------------------------*/
	// full layout
	full_height();
	
	//page tit 
	$(".tit_page").wrapInner( "<span></span>");
	
	//head page title
	if($('.tit_page').size()>0){
		var current_loc = $('.tit_page').text();
		document.title = current_loc + " | LG Chem ESS Battery Division";
	}else{
		document.title = "LG Chem ESS Battery Division";
	}

	//top button
	$("#wrap").filter(function(){
		var $body = $(document.body),
		$top = '';

			$top=$('<div>') 
			.addClass('btn_top') 
			.hide() 
			.click(function(){
				$body.animate({ scrollTop: 0 });
			})
			.appendTo($body); 

		$(window).scroll(function(){

			var y = $(this).scrollTop();

			if(y >= 100){
				$top.fadeIn();
			}else{
				$top.fadeOut();
				}
		});
	});

	$('.btn_ctrl.open').click(function(e){
		e.preventDefault();
		$('body').addClass('on');
	});
	
	$('.btn_ctrl.close').click(function(e){
		e.preventDefault();
		$('body').removeClass('on');
	});

	// design select
	$('.dsg_opt').filter(function(){
		var dsg_list = $(this).find('ul');
		var dsg_link = $(this).find('> * > a');

		dsg_list.hide();	

		$(this).find('h2').on("click", function() {
			dsg_list.toggle();
			$(this).toggleClass('on');
		});	
		$(document).click(function(e) {
	        if (!$(e.target).is('.dsg_opt h2')) {
				$('.dsg_opt h2').removeClass('on');
				dsg_list.hide();
	        }
	    });
	});

	
	/*
		main
	----------------------------------------------------------*/
	// slider
	$(".visual > ul").filter(function(){
		var slider = $('.visual > ul').bxSlider({
			auto: false,
			autoControls: false,
			controls: false,
	        stopAuto: false,
	        startSlide: 0
		 });
		 $(".bx-pager-link").on("click", function(){
		    var i = $(this).attr('data-slide-index');
		    slider.goToSlide(i);
		    slider.stopAuto();
		    return false;
		});
	});
	
	
	/*
		sub
	----------------------------------------------------------*/
	// accordion
	$('.acc_box').filter(function(){
		$('.acc_box').find('.acc_cont').addClass('blind');

		$(this).find('.acc_tit').on('click',function(){
			var me = $(this).next();
			if($('.acc_box').hasClass('qna')){
				$('.acc_cont').not(me).addClass('blind');
			}

			$(this).toggleClass('on');
			$(this).next('.acc_cont').toggleClass('blind');
		});

		
	});

	//tooltip
	$('.tooltip').click(function(e){
		var pos = $(this).position();
		var hgt = $(this).height()+13;
		var pos_t = pos.top;
		//var pos_l = pos.left;
		var tooltip_box = $(this).parents('.tooltip_box').find('.tooltip_txt');
	
		$('.tooltip').not(this).removeClass('on');
		$('.tooltip_txt').not(tooltip_box).removeClass('on');

		tooltip_box.css({'top':pos_t+'px','margin-top':hgt+'px'});
		$(this).toggleClass('on');
		tooltip_box.toggleClass('on');

		e.preventDefault();
	});

	 $(document).click(function(e) {
        if (!$(e.target).is('.tooltip')) {
			$('.tooltip').removeClass('on');
            $(".tooltip_txt").removeClass('on');
        }
    });

	$('div.search').filter(function(){
		$('.search .tab_cont').addClass('blind');

		$('.tab_head').find('li').on('click',function(){
			var i = $(this).index();
			$('.tab_head').find('li').removeClass('on');
			$('.search .tab_cont').addClass('blind');
			$(this).toggleClass('on');
			$('.search .tab_cont').eq(i).toggleClass('blind');
		});

		$('.white').on('click',function(){
			$('.tab_head').find('li').removeClass('on');
			$('.search .tab_cont').addClass('blind');
		});
	});

	//swipe
	if($(".swiper-container").length > 0){
		var initSlide=0;
		var fSwipe=$(".swiper-slide:first-child").hasClass("on");
		var lSwipe=$(".swiper-slide:last-child").hasClass("on");
		
		$(".swiper-slide").each(function(index, item){
			if($(this).hasClass("on")){
				initSlide = index;
				if(fSwipe == true){
					$(".swiper-mask-left").hide();
					$(".swiper-mask-right").show();
				}else if(lSwipe == true ){
					$(".swiper-mask-left").show();
					$(".swiper-mask-right").hide();
				}else{
					$(".swiper-mask-left").show();
					$(".swiper-mask-right").show();
				}
			}
		});
		var swiper = new Swiper('.swiper-container', {
			slidesPerView:'auto',
			freeMode:true,
			initialSlide:initSlide,
			onTouchStart : function() {
				$(".swiper-mask-left").show();
				$(".swiper-mask-right").show();
			}
		});
	};

	


	
});

// full layout
function full_height(){
	var w_height = $(window).height();
	var header_h = $('#header').outerHeight();
	var footer_h = $('#footer').outerHeight();
	var min_height = w_height - (header_h + footer_h) -50;//50 padding

	$("body").filter(function(){		
		$('#contents .cont').css('min-height',min_height);
	});
}

// alert https://andrewensley.com/2012/07/override-alert-with-jquery-ui-dialog/   
window.alert = function(message){
	$(document.createElement('div'))
		.attr({title: 'alert', 'class': 'alert'})
		.html(message)
		.dialog({
			buttons: {OK: function(){$(this).dialog('close');}},
			close: function(){$(this).remove();},
			draggable: true,
			modal: true,
			resizable: false,
			width: 'auto'
	});
	$('.ui-dialog').removeClass('dialog_cont');
};

