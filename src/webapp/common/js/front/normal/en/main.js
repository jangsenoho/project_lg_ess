
$(function(){
    var winW = $(window).outerWidth();
    var $headerWrap = $('.layout-header-wrap');
    var $header = $('.layout-header');
    var $container = $('.layout-container');    
    var $layerReviewWrap = $('.layer-review-wrap');
    var $layerReview = $('.layer-review');
    var $dim = $('.dim');    

    (function(global, $){
        //MAIN VISUAL
        var mainVisualBX = $('.main-visual').bxSlider({
            controls : false,
            auto : true,
            useCSS : false,
            pause : 7000,
            touchEnabled : false,
            onSliderLoad : function(){
                $('.btn-pause').appendTo('.main-visual-wrap .bx-controls');
            }
        });

        //btn play
        $('.btn-pause').on('click',function(){
            if(!$(this).hasClass('play')){
                $(this).addClass('play');
                mainVisualBX.stopAuto();
            }else{
                $(this).removeClass('play');
                mainVisualBX.startAuto();                
            }
        });        

        //Customer Review
        var customer = $('.main-customer-slider').children('li').size();
        if(customer > 1){
            $('.main-customer-slider').bxSlider({
                controls : true,
                auto : false,
                useCSS : false,
                touchEnabled : false,
                pause : 7000
            });
        }        
        
        
        //main RESU lineup        
        $('.main-prdt-list > ul').bxSlider({
            controls : true,
            auto : true,
            useCSS : false,
            touchEnabled : false,
            pause : 7000
        });


        //Video 
        if($('video').size() > 0){
            var player = videojs('myVideo',{
                controls: false,
                autoplay: true,
                preload: 'auto',
                loop : true
            });
            var videoSize = function(winW){
                //video resize
                var videoH;
                if(winW > 1920){
                    winW = 1920;
                    videoH = 784;
                }else{
                    videoH = (winW * 784 / 1920);
                }
                player.dimensions(winW, videoH);
            }
            videoSize(winW);
        }
                
        
                
        var sliderNum = 0;
        var $sliderWrap = $('.slider-wrap');
        var $mainIntroductionWrap = $('.main-introduction-wrap');
        $( "#slider" ).slider({
            min : 0,
            max : 2,
            step : 1,
            slide: function( event, ui ) {                
                var val = ui.value;
                if(val != sliderNum){
                    sliderNum = val;
                    $sliderWrap.removeClass('day night blackout');
                    $mainIntroductionWrap.removeClass('day night blackout');
                    $('.slider-txt').children('span').eq(val).addClass('on').siblings('span').removeClass('on');
                    if(val == 0){
                        $sliderWrap.addClass('day');
                        $mainIntroductionWrap.addClass('day');
                        player.src('/common/movie/case01.mp4').play();
                    }else if(val == 1){
                        $sliderWrap.addClass('night');
                        $mainIntroductionWrap.addClass('night');
                        player.src('/common/movie/case02.mp4').play();
                    }else if(val == 2){
                        $sliderWrap.addClass('blackout');
                        $mainIntroductionWrap.addClass('blackout');
                        player.src('/common/movie/case03.mp4').play();
                    }
                }                
            }
        });   

        //CUSTOMER REVIEW
        $('.reviewer').on('click','a',function(){
            var $parent = $(this).parents('li');
            var $msg = $parent.find('.msg').text();
            $parent.addClass('is-active').siblings('li').removeClass('is-active');
            $('.customer-review-tit .msg').text($msg);
        });

        $('.main-customer-slider').on('click','button',function(){            
            var interview = $(this).next('.interview').html();            
            $dim.fadeIn();
            $('.layer-review').html(interview);
            $layerReviewWrap.fadeIn(function(){
                $layerReview.mCustomScrollbar({
                    theme:"minimal-dark"
                    // ,callbacks:{
                    //     whileScrolling: function(){
                    //         var mct = this.mcs.draggerTop;
                    //         if(mct >= 100){
                    //             $layerReviewWrap.find('.btn-layer-close').addClass('c-black');
                    //         }else{
                    //             $layerReviewWrap.find('.btn-layer-close').removeClass('c-black');
                    //         }
                    //         //console.log($(this),mct)
                    //     }
                    // }
                });
                $('body').on('mousewheel',function(e){
                    e.preventDefault();
                });
            });
        });

        $layerReviewWrap.on('click','.btn-layer-close',function(){
            $layerReview.mCustomScrollbar("destroy").empty();
            $dim.fadeOut();
            $layerReviewWrap.fadeOut(function(){
                $('body').off('mousewheel');
            });
        });        
      

        $(window)
        .on('resize',function(){
            winW = $(this).outerWidth();
            winH = $(this).outerHeight();            
            
            if($('video').size() > 0) videoSize(winW); //video

        });
    })(window, window.jQuery);
});


//메인 안내 팝업 열기
function openFYI(){
    var $layerFYI = $('.layer-fyi');
    var $dim = $('.dim');

    $dim.fadeIn();   
    $layerFYI.fadeIn(function(){
        $('body').on('mousewheel',function(e){
            e.preventDefault();
        });
        //끄기
        $(this).on('click','.close',function(){
            $layerFYI.fadeOut();
            $dim.fadeOut();            
            $('body').off('mousewheel');
        });
    });

}


