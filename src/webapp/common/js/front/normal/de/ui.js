$(function(){
    var $headerWrap = $('.layout-header-wrap');
    var $header = $('.layout-header');
    var $footer = $('.layout-footer');
    
    (function(global, $){
        //GNB
        $header
        .on('mouseenter','.gnb',function(){
            $headerWrap.addClass('submenu-on');
        })
        .on('mouseleave','.gnb',function(){
            $headerWrap.removeClass('submenu-on');
        });

        //lang
        $header.on('click','.lang',function(){
            var $langMenu = $(this);
            if(!$langMenu.hasClass('is-active')){
                $langMenu.addClass('is-active');
            }else{
                $langMenu.removeClass('is-active');
            }            
        });

        //footer        
        var $partnerList = $('.partner-list ul')
        if($partnerList.children('li').size() >1){
            $partnerList.bxSlider({
                controls : false,
                touchEnabled : false,
                auto : true
            });
        }
        
        var $certificationList = $('.certification-list ul');
        if($certificationList.children('li').size() > 1){
            $certificationList.bxSlider({
                controls : false,
                touchEnabled : false,
                auto : true
            });
        }
        

        $('.related-site > a').on('click',function(){
            var $relatedSites = $(this).parent('.related-site');
            var $relatedMenu = $(this).next('ul');
            if(!$relatedSites.hasClass('is-active')){
                $relatedSites.addClass('is-active');
                $relatedMenu.slideDown();
            }else{
                $relatedSites.removeClass('is-active');
                $relatedMenu.slideUp();
            }            
        });

        //Btn-top
        $('.layout-footer').on('click','.btn-top',function(){
            $('html,body').stop().animate({'scrollTop':'0'},400);
        });

        //common
        $('.select-box')
        .on('focusin','select',function(){
            $(this).parent('.select-box').addClass('is-active');            
        })
        .on('focusout','select',function(){
            $(this).parent('.select-box').removeClass('is-active');            
        });

        //datepicker
        var datePicker = $('.datepicker');
        if(datePicker.size() > 0){
            datePicker.children('input[type="text"]').datepicker({
                beforeShow: function(input, inst){
                    //inst.dpDiv.css({marginTop: -input.offsetHeight + 'px', marginLeft: input.offsetWidth + 10 + 'px'});
                },
                dateFormat: "yy/mm/dd"
            });
            datePicker.on('click','button',function(){
                $(this).prev('input').focus();
            });
        }        

        //Accordian
        var $accordianList = $('.accordian-list');
        var $accordianMenu = $accordianList.children('dt');
		
        $accordianMenu.each(function(i){
            if($(this).parents('.qna-list').size() > 0){
                if($(this).children('.answer').size() > 0){
                    $(this).append('<button class="btn-accordian"><span>Show</span></button>');
                }else{
                    $(this).find('a').addClass('cursor-default');
                }
            }else{
                $(this).append('<button class="btn-accordian"><span>Show</span></button>');
            }
			
            if(i >= $accordianMenu.size() - 1){
                $accordianMenu.on('click','.btn-accordian',function(){
                    var $btn = $(this);
                    if(!$btn.hasClass('show-desc')){
                        $accordianList.find('dd').slideUp();
                        $accordianList.find('.btn-accordian').removeClass('show-desc');
                        $btn.addClass('show-desc').children('span').text('Hide');
                        $btn.parents('dt').next('dd').slideDown();
                    }else{
                        $btn.removeClass('show-desc').children('span').text('Show');
                        $btn.parents('dt').next('dd').slideUp();
                    }
                })
                .on('click','a',function(){
                    $(this).parents('dt').find('.btn-accordian').trigger('click');
                });
            }
        });

        //.msg-box
        $('.msg-box')
        .on('click','.msg-call',function(){
            $(this).toggleClass('is-active');
        })
        .on('click','.msg-detail-close',function(){
            $(this).parents('.msg-box').find('.msg-call').toggleClass('is-active');
        });

        //layerpop Close
        $('.layerpop').on('click','.layer-close',function(){
            $(this).parent('.layerpop').fadeOut();
            $('.dim').fadeOut();
        });

        //2줄
        $('.confirm-msg, .error-msg').has('br').addClass('line2');

        //Error msg
        if($('.change-password').size() === 0){ //비밀번호 변경 페이지에서 작동하지 않음
            $('.error-msg').each(function(){
                if($(this).next('.error-msg').size() > 0){
                    $(this).parent().find('.error-msg').wrapAll('<div class="error-msg-wrap">');                
                }
            });
        }

        $('.main-btn-top').on('click',function(){
            $('html,body').stop().animate({'scrollTop':'0'},300);
        });

        //ScrollEvent
        var winH =$(window).outerHeight(); 
        var st = $(window).scrollTop();
        var $quickBtn = $('.main-fixed-btn'); 
        var $quickBtnMember = $('.member-main-fixed'); 
        var btnBtm = (winH - 150)/2;
        var btnBtmMember = (winH - 280)/2;
        //console.log(winH,btnBtm,btnBtmMember)
           

        var scrollEvt = function(st){
            //Footer
            var footerPosition = $('.layout-footer-wrap').offset().top - winH;
            if(st >= footerPosition){
                $quickBtnMember.removeClass('fixed').css({'bottom':btnBtmMember + 'px'});
                $quickBtn.removeClass('fixed').css({'bottom':btnBtm + 'px'});
            }else{
                $quickBtnMember.addClass('fixed').removeAttr('style');
                $quickBtn.addClass('fixed').removeAttr('style');
            }
        }
        //scrollEvt(st);

        $(window)
        .on('scroll',function(){
            st = $(this).scrollTop();
            //scrollEvt(st);
        })
        .on('resize',function(){
            winW = $(this).outerWidth();
            winH = $(this).outerHeight();            
                        
            //installer search
            btnBtm = (winH - 150)/2;
            btnBtmMember = (winH - 280)/2;
            scrollEvt(st);            
        });

    })(window, window.jQuery);
});



