// Window Resize
$(window).bind("resize", function(){
	
	// full layout
	full_height();

});


$(window).on("scroll touchmove", function () {
	// Menu scroll fixed
	$('#wrap').toggleClass('tiny', $(document).scrollTop() > 77);
	
	
	if($(document).scrollTop() > 77){
		$('.menu_quick').animate({top:$(window).scrollTop()-82+"px" },{queue: false, duration: 0});
	}else{
		$('.menu_quick').animate({top:0},{queue: false, duration: 0});
	}

});

$(document).ready(function(){
	
	var device = check_device();
	if(device !=''){
        // 모바일 페이지 이동 및 레이아웃 변경 작업 실행
		$('body').addClass('mob')
    }
	
	/*
		layout
	----------------------------------------------------------*/
	//nav 현재 위치 class
	$('.location').filter(function(){
		var my_url = document.location.href; 
		my_url = my_url.slice(7); 
		arr = my_url.split("/");
		my_url = arr[4];
		$('#nav li li a[href*="'+my_url+'"]').parent('li').addClass('current');
		$('#nav li li.current').parents('#nav > ul > li').addClass('on');
		
		var loc_txt1 = $('.location li').eq(1).text();
		var loc_txt2 = $('.location li').eq(2).text();
		var loc_txt3 = $('.tab_head li.on').text();
		
		if($('#nav').find('li.on').size()==0){
			$('#nav li li').removeClass('current');
			$('#nav').find('li a:contains('+loc_txt1+')').parent('li').addClass('on');
			$('#nav li').find('li a:contains('+loc_txt2+')').parent('li').addClass('current');
		}else if($('#nav li.on').find('li.current').size()>2){
			$('#nav li li').removeClass('current');
			$('#nav li.on').find('li a:contains('+loc_txt3+')').parent('li').addClass('current');
		}

	})
	
	//head page title
	/*if($('.location').find('li').size()>0){
		var current_loc = $('.location').find('li:last').text();
		document.title = current_loc + " | LG Chem ESS Battery Division";
	}else{
		document.title = "LG Chem ESS Battery Division";
	}*/
	
	// full layout
	full_height();

	// nav
	$('#nav').filter(function(){
		var nav = $('#nav');
		var nav_link = nav.find('> ul > li > a');

		nav.find('> ul > li').on("mouseover", function(e) {
			nav.addClass('on');
		});
		
		nav_link.on("mouseover focus", function(e) {
			e.preventDefault();
			if(!nav.hasClass('on')){
				nav.addClass('on');
			}
		});

		nav.on("mouseleave", function(e) {
			nav.removeClass('on');
		});

		nav.find('a:last').on("blur", function(e) {
			nav.removeClass('on');
		});

		nav.find('> ul > li > a:first').keyup(function(e) {
			$(this).each(function() {
				if (e.shiftKey && e.keyCode == 9) {
					
					nav.removeClass('on');
				}
			});
		});

		nav.find('.btn_close a').on("click", function(e) {
			e.preventDefault();
			nav.removeClass('on');
		});
	});
	
	// design select
	$('.dsg_opt').filter(function(){
		var dsg_list = $(this).find('ul');
		var dsg_link = $(this).find('> * > a');

		dsg_list.hide();	

		$(this).on("click focusin", function() {
			dsg_list.slideDown("fast");
		});	
		
		$('.dsg_opt h2 a').on("click focusin", function(e) {
			e.preventDefault();
		});	

		$(this).on("mouseleave", function() {
			dsg_list.slideUp("fast");
		});
		
		dsg_list.find('a:last').on("focusout", function() {
			dsg_list.slideUp("fast");
		});
		
		dsg_link.keyup(function(e) {
			$(this).each(function() {
				if (e.shiftKey && e.keyCode == 9) {
					dsg_list.slideUp("fast");
				}
			});
		});
	});
	
	// header.jsp country design select
	$('.dsg_opt2').filter(function(){
		var dsg_list = $(this).find('ul');
		var dsg_link = $(this).find('> * > a');

		dsg_list.hide();	

		$(this).on("click focusin", function() {
			dsg_list.slideDown("fast");
		});	
		
		$('.dsg_opt2 h2 a').on("click focusin", function(e) {
			e.preventDefault();
		});	

		$(this).on("mouseleave", function() {
			dsg_list.slideUp("fast");
		});
		
		dsg_list.find('a:last').on("focusout", function() {
			dsg_list.slideUp("fast");
		});
		
		dsg_link.keyup(function(e) {
			$(this).each(function() {
				if (e.shiftKey && e.keyCode == 9) {
					dsg_list.slideDown("fast");
				}
			});
		});
	});
	
	/*
		main
	----------------------------------------------------------*/
	// slider
	$(".visual > ul").filter(function(){
		var slider = $('.visual > ul').bxSlider({
			auto: true,
			autoControls: true,
	        stopAuto: true,
	        startSlide: 0
		 });

		$(document).on('click','.bx-pager-link',function() {
	        slider.stopAuto();
	        slider.startAuto();
	    });
		
		$(document).on('click','.bx-stop',function() {
	        $(this).css("display","none");
	        $(".bx-start").css("display","block");
	    });

		$(document).on('click','.bx-start',function() {
	        $(this).css("display","none");
	        $(".bx-stop").css("display","block");
	    });
	});
	
	/*
		sub
	----------------------------------------------------------*/
	
	//tooltip
	$('.tooltip').click(function(e){
		var pos = $(this).position();
		var hgt = $(this).height()+13;
		var pos_t = pos.top;
		//var pos_l = pos.left;
		var tooltip_box = $(this).parents('.tooltip_box').find('.tooltip_txt');
	
		$('.tooltip').not(this).removeClass('on');
		$('.tooltip_txt').not(tooltip_box).removeClass('on');

		tooltip_box.css({'top':pos_t+'px','margin-top':hgt+'px'});
		$(this).toggleClass('on');
		tooltip_box.toggleClass('on');

		e.preventDefault();
	});

	 $(document).click(function(e) {
        if (!$(e.target).is('.tooltip')) {
			$('.tooltip').removeClass('on');
            $(".tooltip_txt").removeClass('on');
        }
    });

	// qna 
	$('.tbl.qna').filter(function(){
		$('tr.as').hide();
		$('.tbl.qna').find('.qc').on('click', function(){
			$('tr.as').not($(this).next()).hide();
			$(this).next('tr.as').toggle();
		});

		$('.tbl.qna').find('.close').on('click', function(){
			$(this).parents('tr.as').hide();
		});
	});
	
	// etc table acc 0315
	$('.acc_tbl').filter(function(){
		$('.acc_txt').addClass('blind');
		$(this).find('.acc_head').on('click', function(){
			$('.acc_head').not($(this)).removeClass('on');
			$('.acc_txt').not($(this).next()).addClass('blind');

			$(this).toggleClass('on');
			$(this).next('.acc_txt').toggleClass('blind');
		});

		$('.acc_tbl').find('.acc_close').on('click', function(){
			$('video').each(function(i) {
				var video = $('video')[i];
				if (!video.paused) {
					video.pause();
				}
			});
			$(this).parents('.acc_txt').addClass('blind');
			$(this).parents('.acc_txt').prev('.acc_head').removeClass('on');
		});
	});

	// produt info 
	$('.acc_box').filter(function(){
		$('.acc_box:not(:first)').find('.acc_cont').addClass('blind');
		$('.acc_box:first').find('.acc_tit').addClass('on');
		$(this).find('.acc_tit').on('click',function(){
			$(this).toggleClass('on');
			$(this).next('.acc_cont').toggleClass('blind');
		});
	});
	
	//design content
	$('.design_cont .article').first().addClass('first');
	
	/*
		form http://jqueryui.com/selectmenu/
	----------------------------------------------------------*/

	// select
/*	$('select').filter(function(){
		$(this).selectmenu();		
	});*/

	// datepicker
	/*$('.datepicker').filter(function(){
		$(this).find('input').datepicker({
		  showOn: 'button',
		  buttonImageOnly:	false,
		  buttonText: "Select date",
		  dateFormat: "yy/mm/dd"
		});
	});*/

});

// full layout
function full_height(){
	var w_height = $(window).height();
	var header_h = $('#header').outerHeight();
	var footer_h = $('#footer').outerHeight();
	var min_height = w_height - (header_h + footer_h);

	$("body").filter(function(){		
		$('#contents .section').css('min-height',min_height);
	});
	
}

//calculator print
function printpage() {
	$('#body').addClass("print_body");
	if (window.print) {
		agree = confirm('Would you like to print ?');
	if (agree) window.print(); 
   }
}

// alert https://andrewensley.com/2012/07/override-alert-with-jquery-ui-dialog/   
window.alert = function(message){
	$(document.createElement('div'))
		.attr({title: 'Notice', 'class': 'alert'})
		.html(message)
		.dialog({
			buttons: {OK: function(){$(this).dialog('close');}},
			close: function(){$(this).remove();},
			draggable: true,
			modal: true,
			resizable: false,
			width: 'auto'
	});
	$('.ui-dialog').removeClass('dialog_cont');
};

//check device
function check_device(){
	var mobileKeyWords = new Array('iPhone', 'iPod', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson');
	var device_name = '';
	for (var word in mobileKeyWords){
		if (navigator.userAgent.match(mobileKeyWords[word]) != null){
			device_name = mobileKeyWords[word];
			break;
		}
	}
return device_name
}