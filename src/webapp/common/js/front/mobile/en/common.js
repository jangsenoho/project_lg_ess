function checkForm() {
	if ($("#loginMemberEmail").val() == "") {		
		alert("Please enter ID (email).");
		$("#loginMemberEmail").focus();
		return;
	}
	if ($("#loginMemberPassword").val() == "") {
		alert("Please enter password.");
		$("#loginMemberPassword").focus();
		return;
	}
	$("#loginForm").submit();
}
function _loginForm(){
	location.href="/front/mobile/en/login/loginForm.lg";
}
function _goMypage() {
	location.href="/front/mobile/en/mypage/myPage.lg";
}
function _logout() {
	location.href="/front/mobile/en/login/logout.lg";
}

function _loginSubmit() {	
	
	if ($("#loginMemberEmail").val() == "") {		
		alert("Please enter ID (email).");
		$("#loginMemberEmail").focus();
		return;
	}
	
	if ($("#loginMemberPassword").val() == "") {
		alert("Please enter password.");
		$("#loginMemberPassword").focus();
		return;
	}
	
	$.ajax({
        type: 'POST',  //포스트방식
        url: sslDomain+'/front/mobile/en/login/login.lg',     //요청처리
        data : 'memberEmail='+$("#loginMemberEmail").val() + "&memberPassword="+$("#loginMemberPassword").val(), //파라미터
        dataType : 'jsonp',      //처리한 요청을 받는 형식
        jsonp : 'getLogin',      //처리한 요청을 받는 형식
        success: function(json){       //성공시 해당함수 실행
        },
        error:function (e){
        	//console.log(e.responseText);
        }
 	});
}

function getLogin(jsonpData) {
	emailSave($("#loginMemberEmail").val());
	
	var result = jsonpData.loginResultCode;
	if (result == "001") { // 로그인성공
		var nUri = document.location.href;
		if(nUri.indexOf('login') >-1){
			location.href = '/front/mobile/en/main/main.lg';
		}else{
			location.href = document.location.href;
		}
	} else if (result == "101") { // 로그인실패(이메일과 비밀번호가 정확하지 않습니다.)
		alert("The email and password do not match.");
	} else if (result == "102") { //로그인실패(이메일을 입력해주세요.)
		alert("Please enter ID (email).");
	} else if (result == "103") { // 로그인실패(비밀번호를 입력해주세요.)
		alert("Please enter password.");
	} else if (result == "201") { // 로그인실패(관리자 승인 대기 중입니다.)
		location.href = essDomain+"/front/mobile/en/login/loginDistyWaitComplete.lg?memberEmail="+$("#loginMemberEmail").val();
	} else if (result == "202") { // 로그인실패(관리자 승인 거부)
		// 해당페이지 이동
		location.href = essDomain+"/front/mobile/en/login/loginDistyNotComplete.lg?memberEmail="+$("#loginMemberEmail").val();
	} else if (result == "301") { // 90일이상 장기 미접속차 처리
		// 해당페이지 이동
		location.href = essDomain+"/front/mobile/en/member/memberLoginLockForm.lg";
	} else if (result == "401") { // 로그인 1회틀렸을때
		alert("Login Failed 1/5 Time(s). If you fail to enter the correct password by the 5th trial,<br>your account will be automatically locked by the system and you have to reset your password.");
	} else if (result == "402") { // 로그인 2회틀렸을때
		alert("Login Failed 2/5 Time(s). If you fail to enter the correct password by the 5th trial,<br>your account will be automatically locked by the system and you have to reset your password.");
	} else if (result == "403") { // 로그인 3회틀렸을때
		alert("Login Failed 3/5 Time(s). If you fail to enter the correct password by the 5th trial,<br>your account will be automatically locked by the system and you have to reset your password.");
	} else if (result == "404") { // 로그인 4회틀렸을때
		alert("Login Failed 4/5 Time(s). If you fail to enter the correct password by the 5th trial,<br>your account will be automatically locked by the system and you have to reset your password.");
	} else if (result == "405") { // 로그인 5회틀렸을때
		location.href = essDomain+"/front/mobile/en/member/memberPasswordSearchForm.lg";
		alert("You have failed to enter the correct password for five times<br>and your account has been automatically locked by the system.<br>Please reset your password.");
	} else if (result == "406") { // 로그인 5회틀린후 로그인성공했을때
		location.href = essDomain+"/front/mobile/en/member/memberPasswordSearchForm.lg";
		alert("You have failed to enter the correct password for five times<br>and your account has been automatically locked by the system.<br>Please reset your password.");
	}
}

function emailSave(email) {
	if ($("#emailSaveCheck").is(":checked")) {
		setCookie('essb2bcookie', email, 100);
	} else {
		setCookie('essb2bcookie', email, -1);
	}
}

function setCookie(cName, cValue, cDay){
    var expire = new Date();
    expire.setDate(expire.getDate() + cDay);
    cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
    if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
    document.cookie = cookies;
}

// 쿠키 가져오기
function getCookie(cName) {
    cName = cName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cName);
    var cValue = '';
    if(start != -1){
        start += cName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cValue = cookieData.substring(start, end);
    }
    return unescape(cValue);
}

function getFileIcon(fileName) {
	var fileExt = fileName.slice(fileName.indexOf(".")+1).toLowerCase();
	var result = "";
	if (fileExt == "pdf") {
		result = "pdf";
	} else if (fileExt == "xls" || fileExt == "xlsx") {
		result = "xls";
	} else if (fileExt == "jpg" || fileExt == "png" || fileExt == "gif" || fileExt == "bmp") {
		if (fileExt == "jpg") {
			result = "jpg";
		} else if (fileExt == "gif") {
			result = "gif";
		}
	} else if (fileExt == "zip") {
		result = "zip";
	} else if (fileExt == "doc" || fileExt == "docx") {
		result = "doc";	
	}
	return result;
}

function pubByteCheckTextarea(oid,tid){
	$(oid).on("keyup",function(){
		var byteTxt = "";
		var byte = function(str){
			var byteNum=0;
			for(i=0;i<str.length;i++){
				byteNum+=(str.charCodeAt(i)>127)?2:1;
				if(byteNum<500){
					byteTxt+=str.charAt(i);
				};
			};
			return Math.round( byteNum );
		};
		if(byte($(this).val())>2000){
			alert("Enter up to 2,000 bytes.");
			$(this).val("");
			$(this).val(byteTxt);
		}else{
			$(tid).html( byte($(this).val()) )
		}
	});
};

$(document).ready(function(){
	
	if (getCookie("essb2bcookie") != "") {
		$("#emailSaveCheck").attr("checked", "checked");
		$("#loginMemberEmail").val(getCookie("essb2bcookie"));
	}
});

function ieVersion () { 
	 var word; 
	 var version = "N/A"; 
	 var agent = navigator.userAgent.toLowerCase(); 
	 var name = navigator.appName; 
	 // IE old version ( IE 10 or Lower ) 
	 if ( name == "Microsoft Internet Explorer" ) word = "msie "; 
	 else { 
		 // IE 11 
		 if ( agent.search("trident") > -1 ) word = "trident/.*rv:"; 
		 // Microsoft Edge  
		 else if ( agent.search("edge/") > -1 ) word = "edge/"; 
	 } 
	 var reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 
	 if (  reg.exec( agent ) != null  ) version = RegExp.$1 + RegExp.$2; 
	 return version; 
} 

// 쿠키 생성
function setCookie(cName, cValue, cDay){
    var expire = new Date();
    expire.setDate(expire.getDate() + cDay);
    cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
    if(typeof cDay != 'undefined') cookies += ';expires=;';
    document.cookie = cookies;
}

// 쿠키 가져오기
function getCookie(cName) {
    cName = cName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cName);
    var cValue = '';
    if(start != -1){
        start += cName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cValue = cookieData.substring(start, end);
    }
    return unescape(cValue);
}

// 쿠키 삭제
function deleteCookie( cookieName )
{
 var expireDate = new Date();
 
 //어제 날짜를 쿠키 소멸 날짜로 설정한다.
 expireDate.setDate( expireDate.getDate() - 1 );
 document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString() + "; path=/";
}
