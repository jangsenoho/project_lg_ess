var toggleCls = function( obj, cls ){
    obj.toggleClass(cls);    
}

$(function(){
	var st = 0;
	var currentSt;
    var winW = $(window).outerWidth();
    var $body = $('body');
    var $header = $('.layout-header');
    var $footer = $('.layout-footer');
    var $sideMenu = $('.layer-menu-wrap');
    var $breadcrumb = $('.breadcrumb');
	var $dim = $('.dim-overlay');
    
    //Header
    $header.on('click','.btn-menu',function(){
		currentSt = st;
        toggleCls($body,'sidemenu-open');
    });

    $sideMenu.on('click','.btn-layer-close',function(){
        toggleCls($body,'sidemenu-open');
		$(window).scrollTop(currentSt);		
    });
    $dim.on('click',function(){
    	$sideMenu.removeClass('.sidemenu-open');
		$(window).scrollTop(currentSt);		
    });
    
    //HELP MESSAGES
    var $helpMsg = $(".help-msg");
    $helpMsg.each(function(){
        var $msgCont = $(this).find('.help-desc');
        if($msgCont.find('.btn-close-help').size() == 0){
            $msgCont.append('<a href="javascript:;" class="btn-close-help"/>');            
        }        
        $('.btn-close-help').click(function(){
            $msgCont.prev('button').trigger('click');
        });
    });
    
    $helpMsg.on('click','button',function(){
        toggleCls($helpMsg,'is-active');                
    });

    $(document).on('click',function(e){
        if($helpMsg.hasClass('is-active')){
            e.stopPropagation();        
            if($helpMsg.has(e.target).length === 0) {
                $helpMsg.find('button').trigger('click');
            }
        }	    
    });
    
    //Breadcrumb
    $breadcrumb
    .on('click','a',function(){
        if(!$(this).parent().is('li') && !$(this).hasClass('home')){
            $(this).parent('div').toggleClass('is-active');
            $(this).parent('div').siblings('div').removeClass('is-active');
        }else{                
            var txt = $(this).text();
            $(this).parents('div').removeClass('is-active').children('a').text(txt);
        }
    })
    .find('div').each(function(){
        if($(this).find('ul').size() === 0){
            $(this).addClass('no-child');
            //$(this).siblings('div').css({'flex':'1.5'});
        }
    });
    
	$(document).on('click',function(e){
	    e.stopPropagation();        
	    if($breadcrumb.has(e.target).length === 0) {
	        $breadcrumb.find('div').removeClass('is-active');
	    }
	});

    //tab
    var tabSet = function(w){        
        var tabWidth = 0;
        var menuWidth = []
        var $tab = $('.tab');
        var $tabMenu = $tab.find('li');
        $tabMenu.each(function(i){
            menuWidth[i] = Math.ceil($(this).outerWidth());
            tabWidth += menuWidth[i];
            if($tabMenu.size() == (i + 1)){
                if(tabWidth > w){
                    $tab.find('ul').css({'width' : tabWidth + 10 + 'px'});                    
                    $tab.find('li').removeAttr('style');
                }else{
                    $tab.find('ul').css({'width':'100%','display':'flex'});
                    $tab.find('li').css({'flex':'1'});
                }   
                $tab.css({'opacity':'1'});
            }        
        });
    }
    setTimeout(function(){ //calculate width after load webfont
        tabSet(winW)
    },200);

    //FAQ
    $('.faq-list').on('click','a',function(){
        var dt = $(this).parent('dt');
        toggleCls(dt,'is-active');
    });   

    
    //Footer slider
    var $businessPartnerList = $('footer .business-partner ul');
    if($businessPartnerList.children('li').size() > 1){
        $businessPartnerList.bxSlider({
            controls : false,
            auto : true,
            useCSS : false
        });
    }
    var $certificationList = $('footer .certification-list ul');
    if($certificationList.children('li').size() > 1){
        $certificationList.bxSlider({
            controls : false,
            auto : true,
            useCSS : false
        });
    }

    //Product Info
    $('.btn-accordian').click(function(){
        toggleCls($(this),'on');
    });

    //SCROLL
    var scrollStatus = 0;
    $(window)
    .on('scroll',function(){        
        st = $(this).scrollTop();

        //Check scroll direction
		if(st > 15){ //safari bounce
			if(st > scrollStatus){
				$header.addClass('minimize');                        
			}else{
				$header.removeClass('minimize');            
			}
		}        
        scrollStatus = st;
    })
    .on('resize',function(){
        winW = $(this).outerWidth();
        tabSet(winW);

    });

	
});





