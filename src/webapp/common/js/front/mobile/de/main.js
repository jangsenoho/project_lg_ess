var $wrap = $('#wrap');
var $dim = $('.dim-overlay');
var $layerpop = $('.layer-review-wrap');
var $mainVisual = $('.main-visual');
var $mainCustomer = $('.main-customer-slider');
var $mainPrdtList = $('.main-prdt-list > ul');


//Main Notice POP SHOW
/*var openFYI = function(){
    $wrap.addClass('is-fixed');
    $dim.addClass('is-show');
    $layerFYI.show();
}*/

$(function(){
    var winW = $(window).outerWidth();
    var st = $(this).scrollTop();
    
    if($mainVisual.children('li').size() > 1){
        $mainVisual.bxSlider({
            controls : false
        });
    }

    //case : day/night/blackout
    $('.case-view').on('click','button',function(){
        var idx = $(this).parent('div').index();
        $(this).parent('div').addClass('on').siblings('div').removeClass('on');
        $('.case-view .txt').children('p').eq(idx).addClass('on').siblings('p').removeClass('on');
        $('.case-img').children('img').eq(idx).addClass('on').siblings('img').removeClass('on');
    });

    //Review        
    if($mainCustomer.children('li').size() > 1){
        $mainCustomer.bxSlider({
            controls : false,
            slideMargin : 15,
            infiniteLoop : false,
            onSliderLoad : function(){
                $(this).parent('.bx-viewport').css('overflow','visible');
            }
        });
    }
    
    var currentSt = 0;
    $mainCustomer.on('click','button',function(){
        var interview = $(this).next('.interview').html();
        $dim.addClass('is-show');
        $layerpop.show().find('.layer-review').html(interview);
        $layerpop.scrollTop(0);
        currentSt = st;
        $wrap.addClass('is-fixed');
    });
    
    //Product List    
    if($mainPrdtList.children('li').size() > 1){
        $mainPrdtList.bxSlider({
            controls : false,
            auto : true,
            pause : 7000
        });
    }
    
    //Layer POP
    $layerpop.on('click','.btn-layer-close',function(){
        $dim.removeClass('is-show');
        $layerpop.hide().find('.layer-review').empty();
        $wrap.removeClass('is-fixed');
        $('html,body').scrollTop(currentSt);
    });
    
    //Hide pop
    $layerFYI.on('click','.btn-layer-close',function(){
        $dim.removeClass('is-show');
        $layerFYI.hide();
        $wrap.removeClass('is-fixed');
    });

    //Youtube
    var youtubeResize = function(winW){
        $('.intro-youtube').css({'height': ((winW - 30)  * 431/766) + 'px'});        
    }
    youtubeResize(winW);


    $(window)
    .on('resize',function(){
        winW = $(this).outerWidth();
        youtubeResize(winW);
    })
    .on('scroll',function(){
        st = $(this).scrollTop();
    });    
	
});