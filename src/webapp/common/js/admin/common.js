// 체크박스 전체 선택
function selectAllCheck(allName, objName) {
	if ($("#"+allName).is(":checked")) {
		$("input[name="+objName+"]").prop("checked", true);	
	} else {
		$("input[name="+objName+"]").prop("checked", false);
	}
}

function fileDel(fileType) {
	$("#"+fileType+"OrgPath").val("");
	$("#"+fileType+"Path").val("");
	$("#"+fileType).replaceWith($("#"+fileType).clone(true));
	$("#"+fileType+"Text").val("");
	$("#"+fileType+"Update").val("N");
	$("#"+fileType+"Btn").html("파일찾기");
	$("#"+fileType+"DelBtn").hide();
}

function trainingFileDel(fileType, num) {
	$("#title"+num).val("");
	$("#"+fileType+"OrgPath").val("");
	$("#"+fileType+"Path").val("");
	$("#"+fileType).replaceWith($("#"+fileType).clone(true));
	$("#"+fileType+"Text").val("");
	$("#"+fileType+"Update").val("N");
	$("#"+fileType+"Btn").html("파일찾기");
	$("#"+fileType+"DelBtn").hide();
}

function compatibleFileDel(fileType, num) {
	$("#title"+num).val("");
	$("#"+fileType+"OrgPath").val("");
	$("#"+fileType+"Path").val("");
	$("#"+fileType).replaceWith($("#"+fileType).clone(true));
	$("#"+fileType+"Text").val("");
	$("#"+fileType+"Update").val("N");
	$("#"+fileType+"Btn").html("파일찾기");
	$("#"+fileType+"DelBtn").hide();
}

// 선택된 체크박스 값조회
function selectCheckValue(objName) {
	var content = "";
	$("input[name="+objName+"]").each(function(index) {
		if ($(this).is(":checked")) {
			if (content != "") {
				content += "#";
			}
			content += $(this).val();
		}
	});
	return content;
}

function ieVersion () { 
	 var word; 
	 var version = "N/A"; 
	 var agent = navigator.userAgent.toLowerCase(); 
	 var name = navigator.appName; 
	 // IE old version ( IE 10 or Lower ) 
	 if ( name == "Microsoft Internet Explorer" ) word = "msie "; 
	 else { 
		 // IE 11 
		 if ( agent.search("trident") > -1 ) word = "trident/.*rv:"; 
		 // Microsoft Edge  
		 else if ( agent.search("edge/") > -1 ) word = "edge/"; 
	 } 
	 var reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 
	 if (  reg.exec( agent ) != null  ) version = RegExp.$1 + RegExp.$2; 
	 return version; 
} 

function toDate(gubun) {
	
	var date = new Date(); 
	var year = date.getFullYear(); 
	var month = new String(date.getMonth()+1); 
	var day = new String(date.getDate()); 

	if(month.length == 1){ 
	  month = "0" + month; 
	} 
	if(day.length == 1){ 
	  day = "0" + day; 
	} 
	return year + gubun + month + gubun + day;
}