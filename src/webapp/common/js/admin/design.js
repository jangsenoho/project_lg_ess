// Window Resize
$(window).bind("resize", function(){
	
	// full layout
	full_height();

});


$(window).on("scroll touchmove", function () {
	// Menu scroll fixed
	$('#wrap').toggleClass('tiny', $(document).scrollTop() > 67);
});

$(document).ready(function(){
	
	/*
		layout
	----------------------------------------------------------*/
	
	//head page title
	if($('.aside').find('li a').hasClass('on')){
		var current_loc = $('.aside').find('li a.on').text();
		document.title = current_loc + " | LG Chem ESS Battery Division";
	}else{
		document.title = "LG Chem ESS Battery Division";
	}
	
	
	// full layout
	full_height();

	// nav
	$('#nav').filter(function(){
		var nav = $('#nav');
		var nav_link = nav.find('> ul > li > a');

		nav.find('> ul > li').on("mouseover", function(e) {
			nav.addClass('on');
		});
		
		nav_link.on("mouseover focus", function(e) {
			e.preventDefault();
			if(!nav.hasClass('on')){
				nav.addClass('on');
			}
		});

		nav.on("mouseleave", function(e) {
			nav.removeClass('on');
		});

		nav.find('a:last').on("blur", function(e) {
			nav.removeClass('on');
		});

		nav.find('> ul > li > a:first').keyup(function(e) {
			$(this).each(function() {
				if (e.shiftKey && e.keyCode == 9) {
					
					nav.removeClass('on');
				}
			});
		});

		nav.find('.btn_close a').on("click", function(e) {
			e.preventDefault();
			nav.removeClass('on');
		});
	});
	
	// design select
	$('.dsg_opt').filter(function(){
		var dsg_list = $(this).find('ul');
		var dsg_link = $(this).find('> * > a');

		dsg_list.hide();	

		$(this).on("click focusin", function(e) {
			e.preventDefault();
			dsg_list.slideDown("fast");
		});	

		$(this).on("mouseleave", function(e) {
			dsg_list.slideUp("fast");
		});
		
		dsg_list.find('a:last').on("focusout", function(e) {
			dsg_list.slideUp("fast");
		});
		
		dsg_link.keyup(function(e) {
			$(this).each(function() {
				if (e.shiftKey && e.keyCode == 9) {
					dsg_list.slideUp("fast");
				}
			});
		});
	});

	
	/*
		form http://jqueryui.com/selectmenu/
	----------------------------------------------------------*/

	// select 버그로 주석 처리
	/*$('select').filter(function(){
		$(this).selectmenu();		
	});*/

	// datepicker
	$('.datepicker').filter(function(){
		$(this).find('input').datepicker({
		  showOn: 'button',
		  buttonImageOnly:	false,
		  buttonText: "Select date",
		  dateFormat: "yy.mm.dd"
		});
	});
	// file 
	$(".file_add").find(".fake_file").bind("change", function() {
        var my_val = $(this).val().replace(/C:\\fakepath\\/i, ''); 
        $(this).parent().find(".fake_val").val(my_val);
     }); 

});

// full layout
function full_height(){
	var w_height = $(window).height();
	var header_h = $('#header').outerHeight();
	var footer_h = $('#footer').outerHeight();
	var min_height = w_height - (header_h + footer_h);

	$("body").filter(function(){		
		$('#contents .section').css('min-height',min_height);
	});
}

//full layout
function loadingimg(){
	$('body').addClass('body_loading');
	$('body').append('<div class="ui-widget-overlay"></div>');		
	$('body').append('<div class="img_loading"></div>');		
}

// alert https://andrewensley.com/2012/07/override-alert-with-jquery-ui-dialog/   
window.alert = function(message){
	$(document.createElement('div'))
		.attr({title: 'alert', 'class': 'dialog-alert-content'})
		.html(message)
		.dialog({
			buttons: {
				OK: function(){
					$(this).dialog('close');
				}
			},
			close: function(){$(this).remove();},
			draggable: true,
			modal: true,
			resizable: false,
			width: 'auto'
	});
	$('.ui-dialog').removeClass('dialog-alert dialog-confirm dialog_cont').addClass('dialog-alert');
};

/**
 * confirm용
 * - TODO : 하나의 로직에서 여러개의 confirm을 사용하는 경우는 여러개로 나누어서 처리해야함.(경우의수가많지 않음.)
 * - callback함수에 true/false에 대한 처리 로직 구현할것.
 * - 아래같은 형식으로 return값을 받아오도록 처리가 불가능합.
 * 		var rtn = confirm('test', function(rtn) { console.log('rtn : ' + rtn ); return rtn});
 * Ref1.)
 * 		confirm('test', function(rtn) { console.log('rtn : ' + rtn ); });
 * 
 * Ref2.)
 * 		function confirmCallback(rtn)  { console.log('rtn : ' + rtn ); }
 * 		confirm('test', confirmCallback);
 */
window.confirm = function(message, callback){
	$(document.createElement('div'))
		.attr({title: 'alert', 'class': 'dialog-confirm-content'})
		.html(message)
		.dialog({
			buttons: [
				{
					text:'Cancel',
					class:'gray',
					click:function(){
						callback(false);
						$(this).dialog('close');
					}
				},
				{
					text: "OK",
					click: function() {
						callback(true);
						$(this).dialog('close');
					}
				}
			],
			close: function(){
				$(this).remove();
			},
			draggable: true,
			modal: true,
			resizable: false,
			width: 'auto'
	});
	$('.ui-dialog').removeClass('dialog-alert dialog-confirm dialog_cont').addClass('dialog-confirm');
};

