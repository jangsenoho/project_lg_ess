//2021.05.20 모바일에서 사용하는 companyaddr배열을 문자열로 특수문자 넣어서 보내주는 로직
/*
 * obj - 등록 폼 div객체
 * divClassNm - 등록폼 아래의 div class명
 * gubn - 구분코드(회원가입 페이지(join)인지 마이페이지(mypage)인지에 따라 구분자(separator) 나눠줘야함 )
 * separator - 문자열 사이에 넣을 구분자
 */
function getCompanyAddrModile(obj, divClassNm, gubn, lan){
	var separator = '#';
	if(gubn == 'mypage'){
		separator = '#!spl#';
	}

	var content ="";
    var index = $(obj).find('.location-wrap[data-id="companyAddress"]').length;
    var radioName ="";		//2021.05.13 Display on [Where to Buy] 추가
    for(var i=0; i<index; i++){
    	radioName = 'displayYn'+(i+1);
        content+="0";              
        content+=separator;
        content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='companyAddress']").eq(i).val();              
        content+=separator;
        content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='companyCity']").eq(i).val();
        content+=separator;
        content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='companyZipcode']").eq(i).val();
        content+=separator;
        if($(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='zipCode']").eq(i).val())
            content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='zipCode']").eq(i).val();                   
        else
            content+="0";
        content+=separator;
        if($(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='lat']").eq(i).val())
            content+=$(obj).find('.location-wrap[data-id="companyAddress"]').find("input[name='lat']").eq(i).val();
        else
            content+="0";
        content+=separator;
        if($(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='lng']").eq(i).val())
            content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='lng']").eq(i).val();
        else
            content+="0";
        content+=separator;
        if(lan=='au'){
        	content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='"+radioName+"']:checked").val();		//2021.05.13 au가 아니라 다른나라일경우 추가
        }else{
        	 if($(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='"+radioName+"']").val()){
             	content+=$(obj).find('.'+divClassNm+'[data-id="companyAddress"]').find("input[name='"+radioName+"']").val();		//2021.05.13 au가 아니라 다른나라일경우 추가
             }else{
             	content+="Y";
             }
        }
        content+=separator;
    }
    $("#companyAddr").val(content);
}
