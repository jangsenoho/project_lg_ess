﻿var currentEmailCheck = "Y";
var currentPasswordCheck = "Y";

var memberEmail1 = "N";
var memberEmail2 = "N";
var memberPassword1 = "N";
var memberPassword2 = "N";
var businessType = "N";
var memberCountry = "N";
var companyName = "N";
var ceoName = "N";
var employeeCount = "N";
var companyPhone = "N";
var companyAddress = "N";
var companyCity = "N";
var companyZipcode = "N";
var preferredDistributor1 = "N";
var preferredDistributor2 = "N";
var zipCode = "N";

var memberEmailCheck = "N";
var companyPhoneCheck = "N";
var companyCheck = "N";

var memberName = "N";

// 영문 숫자 포함 여부 확인
var isU = /[a-z]/;
var isL = /[A-Z]/;
var isN = /[0-9]/;
var isEmployee = /[^0-9]{1,4}/g;
var isPhone = /^[0-9]*$/;
var isID = /^[a-zA-Z0-9][a-zA-Z0-9_\-\.]{0,20}$/;
var isEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,20}))$/;
var isHan = /[ㄱ-ㅎ가-힣]/g;

function inputMsgHide(obj) {
	$("#"+obj).siblings('.error-msg').remove();
	/*$("#"+obj+"Msg").html("");
	$("#"+obj+"Msg").hide();*/
}

function inputMsgShow(obj, objText) {
	$("#"+obj).siblings('.error-msg').remove();
	$("#"+obj).parent().append(objText);
	/*$("#"+obj+"Msg").html(objText);
	$("#"+obj+"Msg").show();*/
}

function checkForm(formId, updateValue) {
	
	if (formId == "memberEmail1") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow("email-domain", "<div class=\"error-msg\">Escriba el correo electrónico ID.</div>");
			currentEmailCheck = "Y";
			return 'N';
		} else {
			currentEmailCheck = "N";
			//inputMsgHide(formId);
		}
		// 영문, 숫자 이외의 글자 확인
		if (!(/^[a-zA-Z0-9][a-zA-Z0-9_\-\.]{0,50}$/).test($("#"+formId).val())) {
			inputMsgShow("email-domain", "<div class=\"error-msg\">Solo hay disponible entre 1 y 20 letras, números y caracteres especiales (_) o (-).</div>");
			currentEmailCheck = "Y";
			return 'N';
		} else {
			currentEmailCheck = "N";
			inputMsgHide("email-domain");
		}
		
		memberEmail1 = "Y";
	}
	
	if (formId == "memberEmail2") {
		// 입력 여부 확인
		if (currentEmailCheck == "N") {
			if ($("#"+formId).val() == "") {
				inputMsgShow("email-domain", "<div class=\"error-msg\">Escriba la dirección de correo electrónico completa.</div>");
				return 'N';
			} else {
				//inputMsgHide("memberEmail1");
			}
			
			// 이메일 형식 확인
			
			if (!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,50}))$/).test($("#memberEmail1").val()+"@"+$("#"+formId).val()) || isHan.test($("#memberEmail1").val()+"@"+$("#"+formId).val())) {
				inputMsgShow("email-domain", "<div class=\"error-msg\">El formato del correo electrónico no es correcto.</div>");
				return 'N';
			} else {
				inputMsgHide('email-domain');
				//inputMsgHide("memberEmail1");
			}
			
			// 이메일 중복여부 확인
			
			if (updateValue != $("#memberEmail1").val()+'@'+$("#"+formId).val()) {
				memberAjax("memberEmail");
			} else {
				memberEmailCheck = "Y";
			}
		}
		
		memberEmail2 = "Y";
	}
	
	if (formId == "memberPassword1") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">Escriba la contraseña.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		}
		
		// 입력 여부 확인
		if ($("#"+formId).val().length < 10 || $("#"+formId).val().length > 20 ) {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">Escriba entre 10 y 20 caracteres para la contraseña.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		}

		if ( ($("#"+formId).val().match(isU) != null || $("#"+formId).val().match(isL) != null) &&  $("#"+formId).val().match(isN) != null) {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		} else {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">Debe incluir letras y números.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		}
		
		//alert($("#"+formId).val().substring(0,1));
		
		// 3자 이상 동일 문사 사용 여부 확인
		var passwordL = $("#"+formId).val().length;
		var passwordTemp1 = "";
		var passwordTemp2 = "";
		//var passwordTemp3 = "";
		var passwordCheck = 0;
		for (var i = 0; i < passwordL; i++) {
			passwordTemp1 = $("#"+formId).val().substring(i,i+1);
			passwordTemp2 = $("#"+formId).val().substring(i+1,i+2);
			//passwordTemp3 = $("#"+formId).val().substring(i+2,i+3);
			if (passwordTemp1 == passwordTemp2){// && passwordTemp1 == passwordTemp3) {
				passwordCheck++;
			}
		}
		
		if ($("#"+formId).val().indexOf("012") != -1 || $("#"+formId).val().indexOf("123") != -1 
				|| $("#"+formId).val().indexOf("234") != -1 || $("#"+formId).val().indexOf("345") != -1
				|| $("#"+formId).val().indexOf("456") != -1 || $("#"+formId).val().indexOf("567") != -1
				|| $("#"+formId).val().indexOf("678") != -1 || $("#"+formId).val().indexOf("789") != -1) {
			passwordCheck++;
		}
		
		if (passwordCheck > 0) {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">No puede usar el mismo número o la misma letra más de una vez.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
			$("#"+formId).addClass("perfect");
			
		}
		memberPassword1 = "Y";
		
		if (updateValue == "Y") {
			if ($("#memberPassword1").val() == "" && $("#memberPassword2").val() == "" ) {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
	}
	
	if (formId == "memberPassword2") {
		if (currentPasswordCheck == "N") {
			// 입력 여부 확인
			if ($("#"+formId).val() == "") {
				inputMsgShow("memberPassword2", "<div class=\"error-msg\">Confirme la contraseña.</div>");
				$("#"+formId).removeClass("perfect");
				return 'N';
			} else {
				inputMsgHide("memberPassword2");
				$("#"+formId).addClass("perfect");
			}
			
			// 동일여부 확인
			if ($("#memberPassword1").val() != $("#"+formId).val()) {
				inputMsgShow("memberPassword2", "<div class=\"error-msg\">La contraseña no coincide.</div>");
				$("#"+formId).removeClass("perfect");
				return 'N';
			} else {
				inputMsgHide("memberPassword2");
				$("#"+formId).addClass("perfect");
			}
		}
		memberPassword2 = "Y";
		
		if (updateValue == "Y") {
			if ($("#memberPassword1").val() == "" && $("#memberPassword2").val() == "" ) {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
	}
	
	if (formId == "memberCountry") {
		if ($("#memberCountry option:selected").val() == "") {
			inputMsgShow("memberCountry", "<div class=\"error-msg\">Seleccione el país.</div>");
			return 'N';
		} else {
			inputMsgHide("memberCountry");
		}
		memberCountry = "Y";
	}
	
	if (formId == "companyName1") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyName1", "<div class=\"error-msg\">Escriba el nombre de la empresa.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 30) {
			inputMsgShow("companyName1", "<div class=\"error-msg\">You cannot enter more than 30 characters for company name.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		companyName = "Y";
	}
	
	if (formId == "companyName2") {
		if ($("#companyName2 option:selected").val() == "") {
			inputMsgShow("companyName2", "<div class=\"error-msg\">Seleccione el nombre de la empresa.<br />Si su empresa no figura en la lista, <a style=\"text-decoration:underline; color:blue;\" href=\"javascript:fnContactus();\">póngase en contacto con nosotros.</a></div>");
			return 'N';
		} else {
			//inputMsgHide("companyName1");
			
			//inputMsgHide(formId);
			/*if (updateValue != $("#"+formId).val()) {
				companyAjax();
			} else {
				companyCheck = "Y";
			}*/
			
			
			if (updateValue != $("#companyName2 option:selected").val()) {
				companyAjax();
			} else {
				memberEmailCheck = "Y";
				inputMsgHide("companyName2");
			}
		}
		companyName = "Y";
	}
	
	if (formId == "companyName3") {
		if ($("#companyName3 option:selected").val() == "") {
			inputMsgShow("companyName3", "<div class=\"error-msg\">Seleccione el nombre de la empresa.<br />Si su empresa no figura en la lista, <a style=\"text-decoration:underline; color:blue;\" href=\"javascript:fnContactus();\">póngase en contacto con nosotros.</a></div>");
			return 'N';
		} else {
			memberEmailCheck = "Y";
			inputMsgHide("companyName3");
		}
		companyName = "Y";
	}
	
	if (formId == "ceoName") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("ceoName", "<div class=\"error-msg\">Escriba el nombre del director ejecutivo.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 30) {
			inputMsgShow("ceoName", "<div class=\"error-msg\">You cannot enter more than 30 characters for CEO name.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		ceoName = "Y";
	}
	
	if (formId == "employeeCount") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("employeeCount", "<div class=\"error-msg\">Especifique el número de empleados.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		if ((/[^0-9]{1,4}/g).test($("#"+formId).val())) {
			inputMsgShow("employeeCount", "<div class=\"error-msg\">Solo puede usar números.</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
			//memberAjax("companyPhone");
		}
		
		if ($("#"+formId).val().length > 4) {
			inputMsgShow("employeeCount", "<div class=\"error-msg\">Solo puede usar hasta 4 dígitos para especificar el número de empleados.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		employeeCount = "Y";
	}
	
	if (formId == "companyPhone" || formId == "companyPhone2") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyPhone", "<div class=\"error-msg\">Escriba el número de teléfono.</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
		}
		
		if (!(/^[0-9]*$/).test($("#"+formId).val())) {
			inputMsgShow("companyPhone", "<div class=\"error-msg\">Solo puede usar números.</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
			if (updateValue != $("#"+formId).val()) {
				memberAjax("companyPhone");
			} else {
				companyPhoneCheck = "Y";
			}
		}
		
		companyPhone = "Y";
	}
	
	if (formId.indexOf("companyAddress") > -1) {
		companyAddress = "N";
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Escriba la dirección.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 100) {
			inputMsgShow(formId, "<div class=\"error-msg\">Es können bis zu 100 Zeichen eingegeben werden.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}		
		companyAddress = "Y";
	}
	
	if (formId.indexOf("companyCity") > -1) {
		companyCity = "N";
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Escriba el nombre de la ciudad.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 100) {
			inputMsgShow(formId, "<div class=\"error-msg\">Se pueden ingresar hasta 100 caracteres.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		companyCity = "Y";
	}
	
	if (formId.indexOf("companyZipcode") > -1) {
		companyZipcode = "N";
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Escriba el código postal.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		/*if (!(/^[0-9]*$/).test($("#"+formId).val())) {
			inputMsgShow(formId, "<div class=\"error-msg\">Solo puede usar números.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		var url =  document.location.href.split("/");
		/*if (url[5] == "en") {
			if ($("#"+formId).val().length  != 4) {
				inputMsgShow("companyZipcode", "<div class=\"error-msg\">Enter the four-digit zip code accurately.</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		} else if (url[5] == "de") {
			if ($("#"+formId).val().length  != 5) {
				inputMsgShow("companyZipcode", "<div class=\"error-msg\">für die PLZ, 5 Zahlen richtig eingeben</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		} */
		
		
		
		companyZipcode = "Y";
	}

	if (formId.indexOf("zipCode") > -1) {
		zipCode="N";
		// 입력 여부 확인
		if ($("#"+formId).val() == "" || $("#lat").val() == "" || $("#lng").val() == "" ) {
			inputMsgShow(formId, "<div class=\"error-msg\">Escriba la dirección o el código postal con exactitud.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if (!(/^[a-zA-Z0-9\s][a-zA-Z0-9_\-\.\s\,\/]{1,100}$/).test($("#"+formId).val())) {
			inputMsgShow(formId, "<div class=\"error-msg\">Escriba el código postal.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		zipCode="Y";
	}
	
	if (formId == "preferredDistributor1") {
		if ($("#preferredDistributor1 option:selected").val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Seleccione el nombre del distribuidor preferido.</div>");
			/*inputMsgHide("preferredDistributor2");
			$("#preferredDistributor2").hide();			*/
			return 'N';
		} else if ($("#preferredDistributor1 option:selected").val() == "Other") {
			$("#preferredDistributor2").show();
			inputMsgHide(formId);
		}else{
			inputMsgHide(formId);
			inputMsgHide("preferredDistributor2");
			$("#preferredDistributor2").hide();
			preferredDistributor2= "Y";
		}
		preferredDistributor1= "Y";
	}
	
	if (formId == "preferredDistributor2") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Please write the name of your preferred distributor.</div>");
			return 'N';
		} else {
			if($("#"+formId).val().length > 30){
				inputMsgShow(formId, "<div class=\"error-msg\">Enter the 30 .</div>");
				return 'N';
			}else{
				inputMsgHide("preferredDistributor2");
			}
		}
		preferredDistributor2="Y";
	}
	
	if (formId == "memberName") {
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Escriba su nombre.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		memberName="Y";
	}
	
}

function businessTypeText(checkValue) {
	if (checkValue == "003" || checkValue == "004") {
		$("div[name='companyArea2']").show();
		$("div[name='companyArea1']").hide();
		$("div[name='companyArea1']").find('.error-msg').remove();
		$("div[name='companyArea2']").find('.error-msg').remove();
		
		$("#memberEmail1").attr("placeholder", "Solo correos electrónicos de empresa");
	} else {
		$("div[name='companyArea1']").show();
		$("div[name='companyArea2']").hide();
		$("div[name='companyArea1']").find('.error-msg').remove();
		$("div[name='companyArea2']").find('.error-msg').remove();
		
		$("#memberEmail1").attr("placeholder", "Se recomienda usar el correo electrónico de la empresa");
		
		if(checkValue == "001"){
			$("#type001").show();
			$("#type002").hide();
		}
		else{
			$("#type002").show();
			$("#type001").hide();
		}
	}
}

function validateMember(addrs) {
	
	//alert($('body,html').scrollTop());
	//alert($("#companyName2 option:selected").val());
	
	memberEmail1 = "N";
	memberEmail2 = "N";
	memberPassword1 = "N";
	memberPassword2 = "N";
	memberCountry = "N";
	companyName = "N";
	ceoName = "N";
	employeeCount = "N";
	companyPhone = "N";
	companyAddress = "N";
	companyCity = "N";
	companyZipcode = "N";
	zipCode = "N";	
	preferredDistributor1 = "N";
	preferredDistributor2 = "N";
	memberName = "N";
	
	if ($("input:radio[name='businessType']:checked").val() == "001" || $("input:radio[name='businessType']:checked").val() == "002") {
		checkForm('memberEmail1');
		checkForm('memberEmail2');
		checkForm('memberPassword1');
		checkForm('memberPassword2');
		checkForm('memberCountry');	
		if ($("input:radio[name='businessType']:checked").val() == "001") { // reseller
			checkForm('companyName1');
			companyCheck = "Y";
		} else { // disty
			checkForm('companyName2');
		}	
		checkForm('ceoName');
		checkForm('employeeCount');
		checkForm('companyPhone');	
		addrs.forEach(function(addr) {
			checkForm(addr);
		});
		/*
		checkForm('companyAddress');
		checkForm('companyCity');
		checkForm('companyZipcode');
		checkForm('zipCode');
		//*/	
		checkForm('preferredDistributor1');
		if($("#preferredDistributor1").val() == "Other"){
			checkForm('preferredDistributor2');
		}
	}
	else{
		checkForm('memberEmail1');
		checkForm('memberEmail2');
		checkForm('memberPassword1');
		checkForm('memberPassword2');
		checkForm('memberName');
		checkForm('companyName3');
	}
	
	
	/*alert("memberEmail1 : " + memberEmail1);
	alert("memberEmail2 : " + memberEmail2);
	alert("memberPassword1 : " + memberPassword1);
	alert("memberPassword2 : " + memberPassword2);
	alert("companyName : " + companyName);
	alert("ceoName : " + ceoName);
	alert("employeeCount : " + employeeCount);
	alert("companyPhone : " + companyPhone);
	alert("companyAddress : " + companyAddress);
	alert("companyCity : " + companyCity);
	alert("companyZipcode : " + companyZipcode);
	alert("memberEmailCheck : " + memberEmailCheck);
	alert("companyPhoneCheck : " + companyPhoneCheck);*/	
	if ($("input:radio[name='businessType']:checked").val() == "001" || $("input:radio[name='businessType']:checked").val() == "002") {
			if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y" && memberCountry == "Y"
				&& companyName == "Y" && ceoName == "Y" && employeeCount == "Y" && companyPhone == "Y"
				&& companyAddress == "Y" && companyCity == "Y" && companyZipcode == "Y" && preferredDistributor1 =="Y" && preferredDistributor2 =="Y" && memberEmailCheck == "Y" && companyPhoneCheck == "Y" && companyCheck == "Y"
			    && zipCode == "Y") {
			
			var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
			$("#memberEmail").val(email);
			var password = $("#memberPassword1").val();
			$("#memberPassword").val(password);
			
			if ($("input:radio[name='businessType']:checked").val() == "001") { // reseller
				$("#companyName").val($("#companyName1").val());
			} else {
				$("#companyName").val($("#companyName2 option:selected").val());
			}
			
			return true;
		} else {
			
			// companyCheck
			
			//$("#companyName1").focus();
			
			if (checkForm('memberEmail1') == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail2') == "N") {
				$("#memberEmail2").focus();
			} else if (memberEmailCheck == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberPassword1') == "N") {
				$("#memberPassword1").focus();
			} else if (checkForm('memberPassword2') == "N") {
				$("#memberPassword2").focus();
			} else if (checkForm('memberCountry') == "N") {
				$("#memberCountry").focus();
			} else if ($("input:radio[name='businessType']:checked").val() == "001" && checkForm('companyName1') == "N") {
				$("#companyName1").focus();
			} else if ($("input:radio[name='businessType']:checked").val() == "002" && checkForm('companyName2') == "N") {
				$("#companyName2").focus();
			} else if ($("input:radio[name='businessType']:checked").val() == "002" && companyCheck == "N") {
				$("#companyTitle").focus();
				$('body,html').animate({
					scrollTop: 400
				}, 0);
			} else if (checkForm('ceoName') == "N") {
				$("#ceoName").focus();
			} else if (checkForm('employeeCount') == "N") {
				$("#employeeCount").focus();
			} else if (checkForm('companyPhone') == "N") {
				$("#companyPhone").focus();
			} 
			/*
			else if (checkForm('companyAddress') == "N") {
				$("#companyAddress").focus();
			} else if (checkForm('companyCity') == "N") {
				$("#companyCity").focus();
			} else if (checkForm('companyZipcode') == "N") {
				$("#companyZipcode").focus();
			} else if (checkForm('zipCode') == "N") {
				$("#zipCode").focus();
			} 
			//*/
			else if (checkForm('preferredDistributor1') == "N") {
				$("#preferredDistributor1").focus();
			} else if ($("#preferredDistributor1").val() == "Other") {
			    if (checkForm('preferredDistributor2') == "N") {
					$("#preferredDistributor2").focus();
				}
			} 
			else{
				addrs.some(function(addr) {
					if(checkForm(addr) == "N"){
						$("#"+ addr).focus();
						return true;
					}
				});
			}
			
			var browser = ieVersion();
			if (Number(browser) > 9) {
				$('body,html').animate({
					scrollTop: $('body,html').scrollTop()-150
				}, 0);
			}
			
			return false;
		}
	}
	else{
		if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y" && memberName == "Y") {
			var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
			$("#memberEmail").val(email);
			var password = $("#memberPassword1").val();
			$("#memberPassword").val(password);
			
			$("#esdCompanyCode").val($("#companyName3 option:selected").val());
			$("#companyName").val($("#companyName3 option:selected").text());
			
			return true;
		}
		else{
			if (memberEmailCheck == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail1') == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail2') == "N") {
				$("#memberEmail2").focus();
			} else if (checkForm('memberPassword1') == "N") {
				$("#memberPassword1").focus();
			} else if (checkForm('memberPassword2') == "N") {
				$("#memberPassword2").focus();
			} else if (checkForm('memberName') == "N") {
				$("#memberName").focus();
			} else if (checkForm('companyName3') == "N") {
				$("#companyName3").focus();
			}
			
			var browser = ieVersion();
			if (Number(browser) > 9) {
				$('body,html').animate({
					scrollTop: $('body,html').scrollTop()-150
				}, 0);
			}
			
			return false;
		}
	}	
}

function validateMember2(addrs) {
	
	memberEmail1 = "N";
	memberEmail2 = "N";
	memberPassword1 = "N";
	memberPassword2 = "N";
	memberCountry = "N";
	companyName = "N";
	ceoName = "N";
	employeeCount = "N";
	companyPhone = "N";
	companyAddress = "N";
	companyCity = "N";
	companyZipcode = "N";
	zipCode = "N";	
	
	memberEmailCheck = "N";
	companyPhoneCheck = "N";
	companyCheck = "N";
	
	checkForm('memberEmail1');
	checkForm('memberEmail2', $("input[name='memberEmail']").val());
	if ($("#memberPassword1").val() != "" || $("#memberPassword2").val() != "" ) {
        checkForm('memberPassword1', 'Y');
        checkForm('memberPassword2', 'Y');
    } else {
        memberPassword1 = "Y";
        memberPassword2 = "Y";
    }
	checkForm('memberCountry');	
	if ($("input[name='businessType']").val() == "001") { // reseller
		checkForm('companyName1');
		companyCheck = "Y";
	} else { // disty
		companyCheck = "Y";
		companyName = "Y";
	}	
	checkForm('ceoName');
	checkForm('employeeCount');
	checkForm('companyPhone', $("input[name='companyPhone']").val());	
	addrs.forEach(function(addr) {
		checkForm(addr);
	});

	if ($("input[name='businessType']").val() == "001" || $("input[name='businessType']").val() == "002") {
			if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y" && memberCountry == "Y"
				&& companyName == "Y" && ceoName == "Y" && employeeCount == "Y" && companyPhone == "Y"
				&& companyAddress == "Y" && companyCity == "Y" && companyZipcode == "Y" && memberEmailCheck == "Y" && companyPhoneCheck == "Y" && companyCheck == "Y"
			    && zipCode == "Y") {
			
			var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
			$("#memberEmail").val(email);
			var password = $("#memberPassword1").val();
			$("#memberPassword").val(password);
			
			if ($("input[name='businessType']").val() == "001") { // reseller
				$("#companyName").val($("#companyName1").val());
			} else {
				$("#companyName").val($("#companyName2 option:selected").val());
			}
			
			return true;
		} else {

			if (checkForm('memberEmail1') == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail2', $("input[name='memberEmail']").val()) == "N") {
				$("#memberEmail2").focus();
			} else if (memberEmailCheck == "N") {
				$("#memberEmail1").focus();
			} else if ($("#memberPassword1").val() != "" || $("#memberPassword2").val() != "" ) {
	            
	            if ($("#memberPassword1").val() != "" || $("#memberPassword2").val() != "" ) {
	                
	                if (checkForm('memberPassword1') == "N") {
	                    $("#memberPassword1").focus();
	                } else if (checkForm('memberPassword2') == "N") {
	                    $("#memberPassword2").focus();
	                }
	            } else {
	                memberPassword1 = "Y";
	                memberPassword2 = "Y";
	            }
			} else if (checkForm('memberCountry') == "N") {
				$("#memberCountry").focus();
			} else if ($("input[name='businessType']").val() == "001" && checkForm('companyName1') == "N") {
				$("#companyName1").focus();
			} else if ($("input[name='businessType']").val() == "002" && checkForm('companyName2') == "N") {
				$("#companyName2").focus();
			} else if ($("input[name='businessType']").val() == "002" && companyCheck == "N") {
				$("#companyTitle").focus();
				$('body,html').animate({
					scrollTop: 400
				}, 0);
			} else if (checkForm('ceoName') == "N") {
				$("#ceoName").focus();
			} else if (checkForm('employeeCount') == "N") {
				$("#employeeCount").focus();
			} else if (checkForm('companyPhone', $("input[name='companyPhone']").val()) == "N") {
				$("#companyPhone").focus();
			} 
			else{
				addrs.some(function(addr) {
					if(checkForm(addr) == "N"){
						$("#"+ addr).focus();
						return true;
					}
				});
			}
			
			var browser = ieVersion();
			if (Number(browser) > 9) {
				$('body,html').animate({
					scrollTop: $('body,html').scrollTop()-150
				}, 0);
			}
			
			return false;
		}
	}
}

function memberAjax(selectWhere) {	
	
	var param = "";
	if (selectWhere == "memberEmail") {
		param = "memberEmail="+$("#memberEmail1").val() + "@" + $("#memberEmail2").val();
	} else if (selectWhere == "companyPhone") {
		param = "companyPhone="+$("#companyPhone").val();
	}
	
	$.ajax({
        type: 'POST',
        url: '/es/membership/memberFormCheck.lg',
        data : param + '&selectWhere='+selectWhere,
        dataType : 'xml',
        async:false,
        success: function(json){
        	
        	var result = $(json).find("result").text();
        	var selectWhere = $(json).find("selectWhere").text();
        	// 이메일 중복 체크
        	if (selectWhere == "memberEmail") {
        		if (result == 0) {
        			inputMsgHide("memberEmail1");
        			memberEmailCheck = "Y";
        		} else {
        			var altContent = "";
        			altContent += "<div class=\"error-msg\">Ya existe una cuenta. Escriba otra dirección de correo electrónico.";
        			altContent += "</div>";
        			inputMsgShow("email-domain", altContent);
        			memberEmailCheck = "N";
        		}
        	} else if (selectWhere == "companyPhone") {
        		if (result == 0) {
        			inputMsgHide("companyPhone");
        			companyPhoneCheck = "Y";
        		} else {
        			var memberEmail = $(json).find("memberEmail").text();
        			memberEmail = memberEmail.substring(0,1) + "**********";
        			
        			var altContent = "";
        			altContent+= "<div class=\"error-msg\">Este número de teléfono ya está registrado para "+memberEmail+"";
        			altContent+= "	<a href=\"javascript:fnLogin();\">Iniciar sesión</a>";
        			altContent+= "	<a href=\"javascript:fnResetPwd();\">Restablecer contraseña</a>";
        			altContent+= "</div>";
        			
        			inputMsgShow("companyPhone", altContent);
        			companyPhoneCheck = "N";
        		}
        	}
        },
        error:function (e){
        	alert(e.responseText);
        }
 	});
}

function companyAjax() {	
	
	$.ajax({
        type: 'POST',
        url: '/es/membership/memberFormCompanyCheck.lg',
        data : 'companyName='+$("#companyName2").val(),
        dataType : 'xml',
        async:false,
        success: function(json){
        	
        	var result = $(json).find("result").text();
        	var companySeq = $(json).find("seq").text();
        	var companyEmail = $(json).find("memberEmail").text();
        	if (companySeq == "") {
        		inputMsgHide("companyName2");
        		companyCheck = "Y";
        	} else {
        		var altContent = "";
    			altContent += "<div class=\"error-msg\">Las compañías seleccionadas ya están instaladas (ID : "+companyEmail+")";
    			altContent += "	<a href=\"javascript:fnLogin();\">Iniciar sesión</a>";
    			altContent += "	<a href=\"javascript:fnContactus();\">Contacto</a>";
    			altContent += "</div>";
        		inputMsgShow("companyName2", altContent);
        		companyCheck = "N";
        	}
        },
        error:function (e){
        	alert(e.responseText);
        }
 	});
}
