var productSwiper;

function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
$( function(){
    /*** All Page ***/
    // scoll event
    var lastScrollTop = 51;
    $(window).on('scroll',function(event){
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            $('body').addClass('scroll-down');
            $('body').removeClass('scroll-up');
        }else{
            $('body').addClass('scroll-up');
            $('body').removeClass('scroll-down');
        }
        if(st > 51){
            lastScrollTop = st;
        }
        var winH = $(window).outerHeight();
        var wrapH = $('.l-wrap').outerHeight();
        var totalH = wrapH - winH;
        if(st > totalH - 129){
            $('.l-quick-menu').addClass('nofloating');
        }else{
            $('.l-quick-menu').removeClass('nofloating');
        }
    });

    //top button
    var winTop = $(window).scrollTop();
    $('.btn-top').on('click', function(){
        $('html, body').animate({scrollTop: 0}, 200);
    });
    
    // gnb sub menu open & close
    var $thsHeader = $('.l-header');
    $thsHeader.find('.gnb-list-tit').on('click', function(){
        if( $(this).parent('li').hasClass('is-close') ){
            $(this).parent('li').removeClass('is-close');
            $(this).siblings('ul').slideDown();
        }else{
            $(this).parent('li').addClass('is-close');
            $(this).siblings('ul').slideUp();
        }
    })
    // breadcrhumb
    var depth_type = $('.l-breadcrumb-wrap').children('.breadcrumb').attr('depth-type');
    var single_open = $('.l-breadcrumb-wrap').find('.depth01').attr('single-open');
    if(depth_type == 'single'){
        $('.l-breadcrumb-wrap').find('.depth02').remove();
    }
    if(depth_type == 'single-sub'){
        $('.l-breadcrumb-wrap').find('.depth01').remove();
    }
    if(single_open == 'close'){
        $('.l-breadcrumb-wrap').find('.depth01').children('.sub-list').remove();
    }
    $('.l-breadcrumb-wrap').find('[class*="-menu"]').each( function(n){
        if($(this).siblings('ul').length > 0){
            var flag = true;
            $(this).on('click', function(){
                if(flag){
                    $(this).addClass('is-active').siblings('.sub-list, ul.is-active').slideDown('fast');
                    $(this).parents('.l-container').addClass('dim');
                    $('body').addClass('is-fixed');
                    flag = false;
                }else{
                    $(this).removeClass('is-active').siblings('.sub-list, ul.is-active').slideUp('fast');
                    flag = true;
                };
                if($('.l-breadcrumb-wrap').find('[class*="-menu"].is-active').length < 1){
                    $(this).parents('.l-container').removeClass('dim');
                    $('body').removeClass('is-fixed');
                }
                // dim area click
                $('.l-container').on('click', function(e) {
                    if($(e.target).hasClass('dim')){
                        $('.l-breadcrumb-wrap').find('[class*="-menu"].is-active').trigger('click');
                    }
                });
            });
        }
    });

    /*** Common ***/
    // layer menu open
    $('.layer-menu-btn').on('click', function(){
        var btnChk_Lang = $(this).hasClass('btn-language');
        var bntChk_Gnb = $(this).hasClass('btn-gnb');
        var bntChk_Filter = $(this).hasClass('filter-btn');
        if(bntChk_Gnb){
            $('.gnb-wrap').addClass('is-active');
        }
        if(btnChk_Lang){
            $('.language-wrap').addClass('is-active');
        }
        if(bntChk_Filter){
            $('.l-section').find('.filter-layer').addClass('is-active').parents('.l-section').addClass('dim');
            // filter dim click
            $('.filter-layer').on('click', function(e) {
                if($(e.target).hasClass('filter-layer')){
                    $(this).find('.close-layer-btn').trigger('click');
                }
            });
            //filter select
            $('.filter-layer .main-filter').find('input:radio').each( function(){
                $(this).change( function(){
                    var thsId = $(this).attr('id');
                    var checked = $(this).prop('checked');
                    if(checked){
                        $('.filter-layer .sub-filter-list').children('ul[filter-value="' + thsId + '"]').addClass('is-active').siblings('ul').removeClass('is-active');
                    }
                });
            });
        }
        $('body').addClass('is-fixed');
    });
    // layer menu close
    $('.layer-menu .close-layer-btn').on('click', function(){
        var layerChk_Gnb = $(this).parents('.layer-menu').hasClass('gnb-wrap');
        if(layerChk_Gnb){
            $(this).siblings('ul').children('li').removeClass('is-active');
//            $(this).parents('.gnb-wrap').removeClass('is-login');
        }
        $(this).parents('.layer-menu').removeClass('is-active');
        $('.l-section').removeClass('dim');
        $('body').removeClass('is-fixed');
    });
    // layer Popup
    // layer popup active
    $('.layer-popup-btn').on('click', function(){
        var thsValue = $(this).attr('layer-name');
        $('.' + thsValue).addClass('is-active');
        if($('.l-layer-popup.is-active').length > 0){
            $('body').addClass('is-fixed');
        }
        //product info swiper
        if($('.product-swiper').length > 0){
            productSwiper = new Swiper('.product-swiper', {
                loop: 'true',
                slidesPerView: 'auto',
                navigation: {
                    nextEl: '.product-next',
                    prevEl: '.product-prev',
                },
                simulateTouch:false,
                grabCursor:false,
                touchRatio: 0
            });
        }
    });
    //layer popup close
    $('.l-layer-popup .close-layer-btn').on('click', function(){
        $(this).parents('.l-layer-popup').removeClass('is-active');
        $('body').removeClass('is-fixed');
        if($('.product-swiper').length > 0){
            productSwiper.destroy();
        }
    });
    
    //tooltip
    $('.tooltip-btn').on('click', function(){
        var popupChk = $(this).parents('.tooltip-area').find('.tooltip-layer');
        if( popupChk.length > 0 ){
            //small layer popup
            popupChk.addClass('is-active');
            popupChk.find('.close-layer-btn').on('click', function(){
                $(this).parent('.tooltip-layer').removeClass('is-active');
            });
        }else{
            //layer popup
            var thsValue = $(this).attr('tooltip-value');
            $('.' + thsValue).addClass('is-active');
            $('body').addClass('is-fixed');
        }
    });
    // arcodian
    $('.arcodian').children('.arcodian-list').each( function(){
        // agree-check-list
        if($(this).hasClass('agree-check-list')){
            $(this).find('.more-view').on('click', function(){
                if($(this).parents('.agree-check-list').attr('state') !== 'disabled'){
                    if( $(this).parents('.agree-check-list').hasClass('is-active') ){
                        $(this).parents('.agree-check-list').removeClass('is-active').children('.arcodian-content').slideUp();
                    }else{
                        $(this).parents('.agree-check-list').addClass('is-active').children('.arcodian-content').slideDown();
                        $(this).parents('.agree-check-list').siblings().removeClass('is-active').children('.arcodian-content').slideUp();
                    }
                }
                console.log('trigger');
            });
        }else{
            $(this).children('.arcodian-tit').on('click', function(){
                if( $(this).parent().attr('state') !== 'disabled'){
                    if( $(this).parent().hasClass('is-active') ){
                        $(this).parent().removeClass('is-active').children('.arcodian-content').slideUp();
                    }else{
                        $(this).parent().addClass('is-active').children('.arcodian-content').slideDown();
                        $(this).parent().siblings().removeClass('is-active').children('.arcodian-content').slideUp();
                    }
                }
            });
        }
    });
    //file-list-swiper-menu
    var swiperMenu = $('.swiper-menu');
    if(swiperMenu.length > 0){
        var listMenuSwiper = new Swiper('.swiper-menu', {
            slidesPerView: 'auto',
            spaceBetween: 20,
            grabCursor : true,
            loop: false,
            slideToClickedSlide: true
        });
        swiperMenu.find('li').each( function(){
            $(this).on('click', function(){
                $(this).addClass('is-active').siblings('li').removeClass('is-active');
            })
        });
    }
    //datepicker
    var datePicker = $('.datepicker');
    var datePickerBox = $('.ui-datepicker');
    if(datePicker.length > 0){
        datePicker.children('input[type="text"]').datepicker({
            dateFormat: "dd.mm.yy"
        });
        datePicker.on('click','button',function(){
            $(this).prev('input').focus();
        });
    }
    //Password-visibility
    $('.password').find('.password-visibility').on('click', function(){
        $(this).toggleClass('show');
        if($(this).siblings('.input-pw').attr('type') === 'password'){
            $(this).siblings('.input-pw').attr('type', 'text');
        }else{
            $(this).siblings('.input-pw').attr('type', 'password');
        }
    });
    //Email Input Special Character
    // ID
    var emailId = $('.email-id');
    if(emailId.length > 0){
        emailId.children('input').on('keypress', function (event) {
            var regex_id = /[~!@\#$%^&*\()\=+',]/gi;
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (regex_id.test(key)) {
                event.preventDefault();
                alert('_, -, .을 제외한 특수문자는 입력하실 수 없습니다!');
                return false;
            }
        });
    }
    // Domain
    var emailDomain = $('.email-domain');
    if(emailDomain.length > 0){
        emailDomain.children('input').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9.]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                alert('특수문자는 입력하실 수 없습니다!');
                return false;
            }
        });
    }

    //Product List View Change
    $('.arrange-btn').each( function(){
        $(this).on('click', function(){
            var chk_active = $(this).hasClass('is-active');
            var chk_grid = $(this).hasClass('grid');
            if(chk_active){
                $(this).removeClass('is-active');
            }else{
                $(this).addClass('is-active').siblings('.arrange-btn').removeClass('is-active');
                if(chk_grid){
                    $('.prd-list-wrap').removeClass('type-list').addClass('type-grid');
                }else{
                    $('.prd-list-wrap').removeClass('type-grid').addClass('type-list');
                }
            }
        });
    });
});
$(document).ready( function(){
    if(isMobile){
        minHight();
        $(window).on('resize', function(){
            minHight();
        });
    }
});

//section min-height
function minHight(){
    var exceptHt = Number($('.l-wrap').css('padding-top').replace(/[^-\d\.]/g, '')) + $('.l-footer-wrap').height();
    var targetHt = $('.l-section').height();
    var winHt = $(window).height();
    var htMargin = winHt - exceptHt - targetHt;
    var pgHt = $('.l-section').find('.pagination-area').outerHeight();
    if(!$('.l-wrap').hasClass('main')){
    	if($('.l-section').find('.pagination-area').length > 0){
    		$('.l-section').css('min-height', winHt - exceptHt + pgHt);
    	}else{
    		$('.l-section').css('min-height', winHt - exceptHt);
    	}
    }
}