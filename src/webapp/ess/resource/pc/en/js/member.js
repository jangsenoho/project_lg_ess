﻿var currentEmailCheck = "Y";
var currentPasswordCheck = "Y";

var memberEmail1 = "N";
var memberEmail2 = "N";
var memberPassword1 = "N";
var memberPassword2 = "N";
var businessType = "N";
var memberCountry = "N";
var companyName = "N";
var ceoName = "N";
var employeeCount = "N";
var companyPhone = "N";
var companyAddress = "N";
var companyCity = "N";
var companyZipcode = "N";
var preferredDistributor1 = "N";
var preferredDistributor2 = "N";
var zipCode = "N";

var memberEmailCheck = "N";
var companyPhoneCheck = "N";
var companyCheck = "N";

var memberName = "N";

// 영문 숫자 포함 여부 확인
var isU = /[a-z]/;
var isL = /[A-Z]/;
var isN = /[0-9]/;
var isEmployee = /[^0-9]{1,4}/g;
var isPhone = /^[0-9]*$/;
var isID = /^[a-zA-Z0-9][a-zA-Z0-9_\-\.]{0,20}$/;
var isEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,20}))$/;
var isHan = /[ㄱ-ㅎ가-힣]/g;

function inputMsgHide(obj) {
	$("#"+obj).siblings('.error-msg').remove();
	/*$("#"+obj+"Msg").html("");
	$("#"+obj+"Msg").hide();*/
}

function inputMsgShow(obj, objText) {
	$("#"+obj).siblings('.error-msg').remove();
	$("#"+obj).parent().append(objText);
	/*$("#"+obj+"Msg").html(objText);
	$("#"+obj+"Msg").show();*/
}

function checkForm(formId, updateValue) {
	
	if (formId == "memberEmail1") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Enter email ID.</div>");
			currentEmailCheck = "Y";
			return 'N';
		} else {
			currentEmailCheck = "N";
			//inputMsgHide(formId);
		}
		// 영문, 숫자 이외의 글자 확인
		if (!(/^[a-zA-Z0-9][a-zA-Z0-9_\-\.]{0,50}$/).test($("#"+formId).val())) {
			inputMsgShow(formId, "<div class=\"error-msg\">1-20 of alphbet, numbers and special characters (_), (-) is only available.</div>");
			currentEmailCheck = "Y";
			return 'N';
		} else {
			currentEmailCheck = "N";
			inputMsgHide(formId);
		}
		
		memberEmail1 = "Y";
	}
	
	if (formId == "memberEmail2") {
		// 입력 여부 확인
		if (currentEmailCheck == "N") {
			if ($("#"+formId).val() == "") {
				inputMsgShow("memberEmail1", "<div class=\"error-msg\">Enter the full email address.</div>");
				return 'N';
			} else {
				//inputMsgHide("memberEmail1");
			}
			
			// 이메일 형식 확인
			
			if (!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,50}))$/).test($("#memberEmail1").val()+"@"+$("#"+formId).val()) || isHan.test($("#memberEmail1").val()+"@"+$("#"+formId).val())) {
				inputMsgShow("memberEmail1", "<div class=\"error-msg\">Email format is incorrect.</div>");
				return 'N';
			} else {
				//inputMsgHide("memberEmail1");
			}
			
			// 이메일 중복여부 확인
			
			if (updateValue != $("#memberEmail1").val()+'@'+$("#"+formId).val()) {
				memberAjax("memberEmail");
			} else {
				memberEmailCheck = "Y";
			}
		}
		
		memberEmail2 = "Y";
	}
	
	if (formId == "memberPassword1") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">Please enter password.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		}
		
		// 입력 여부 확인
		if ($("#"+formId).val().length < 10 || $("#"+formId).val().length > 20 ) {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">Enter 10-20 characters for the password.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		}

		if ( ($("#"+formId).val().match(isU) != null || $("#"+formId).val().match(isL) != null) &&  $("#"+formId).val().match(isN) != null) {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
		} else {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">Alphabet and number must be included.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		}
		
		//alert($("#"+formId).val().substring(0,1));
		
		// 3자 이상 동일 문사 사용 여부 확인
		var passwordL = $("#"+formId).val().length;
		var passwordTemp1 = "";
		var passwordTemp2 = "";
		//var passwordTemp3 = "";
		var passwordCheck = 0;
		for (var i = 0; i < passwordL; i++) {
			passwordTemp1 = $("#"+formId).val().substring(i,i+1);
			passwordTemp2 = $("#"+formId).val().substring(i+1,i+2);
			//passwordTemp3 = $("#"+formId).val().substring(i+2,i+3);
			if (passwordTemp1 == passwordTemp2){// && passwordTemp1 == passwordTemp3) {
				passwordCheck++;
			}
		}
		
		if ($("#"+formId).val().indexOf("012") != -1 || $("#"+formId).val().indexOf("123") != -1 
				|| $("#"+formId).val().indexOf("234") != -1 || $("#"+formId).val().indexOf("345") != -1
				|| $("#"+formId).val().indexOf("456") != -1 || $("#"+formId).val().indexOf("567") != -1
				|| $("#"+formId).val().indexOf("678") != -1 || $("#"+formId).val().indexOf("789") != -1) {
			passwordCheck++;
		}
		
		if (passwordCheck > 0) {
			inputMsgShow("memberPassword1", "<div class=\"error-msg\">You cannot use the same number or alphabet repeatedly.</div>");
			currentPasswordCheck = "Y";
			$("#"+formId).removeClass("perfect");
			return 'N';
		} else {
			currentPasswordCheck = "N";
			inputMsgHide(formId);
			$("#"+formId).addClass("perfect");
			
		}
		memberPassword1 = "Y";
		
		if (updateValue == "Y") {
			if ($("#memberPassword1").val() == "" && $("#memberPassword2").val() == "" ) {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
	}
	
	if (formId == "memberPassword2") {
		if (currentPasswordCheck == "N") {
			// 입력 여부 확인
			if ($("#"+formId).val() == "") {
				inputMsgShow("memberPassword2", "<div class=\"error-msg\">Please enter confirm password.</div>");
				$("#"+formId).removeClass("perfect");
				return 'N';
			} else {
				inputMsgHide("memberPassword2");
				$("#"+formId).addClass("perfect");
			}
			
			// 동일여부 확인
			if ($("#memberPassword1").val() != $("#"+formId).val()) {
				inputMsgShow("memberPassword2", "<div class=\"error-msg\">Password does not match.</div>");
				$("#"+formId).removeClass("perfect");
				return 'N';
			} else {
				inputMsgHide("memberPassword2");
				$("#"+formId).addClass("perfect");
			}
		}
		memberPassword2 = "Y";
		
		if (updateValue == "Y") {
			if ($("#memberPassword1").val() == "" && $("#memberPassword2").val() == "" ) {
				inputMsgHide("memberPassword1");
				$("#"+formId).addClass("perfect");
			}
		}
	}
	
	if (formId == "memberCountry") {
		if ($("#memberCountry option:selected").val() == "") {
			inputMsgShow("memberCountry", "<div class=\"error-msg\">Select the country.</div>");
			return 'N';
		} else {
			inputMsgHide("memberCountry");
		}
		memberCountry = "Y";
	}
	
	if (formId == "companyName1") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyName1", "<div class=\"error-msg\">Enter the name of the company.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 30) {
			inputMsgShow("companyName1", "<div class=\"error-msg\">You cannot enter more than 30 characters for company name.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		companyName = "Y";
	}
	
	if (formId == "companyName2") {
		if ($("#companyName2 option:selected").val() == "") {
			inputMsgShow("companyName2", "<div class=\"error-msg\">Select the name of the company.<br />If your company is not listed you can <a style=\"text-decoration:underline; color:blue;\" href=\"javascript:fnContactus();\" class=\"btn inner\">Apply for distributor</a>.</div>");
			return 'N';
		} else {
			//inputMsgHide("companyName1");
			
			//inputMsgHide(formId);
			/*if (updateValue != $("#"+formId).val()) {
				companyAjax();
			} else {
				companyCheck = "Y";
			}*/
			
			
			if (updateValue != $("#companyName2 option:selected").val()) {
				companyAjax();
			} else {
				memberEmailCheck = "Y";
				inputMsgHide("companyName2");
			}
		}
		companyName = "Y";
	}
	
	if (formId == "companyName3") {
		if ($("#companyName3 option:selected").val() == "") {
			inputMsgShow("companyName3", "<div class=\"error-msg\">Select the name of the company.<br />If your company is not listed you can <a style=\"text-decoration:underline; color:blue;\" href=\"javascript:fnContactus();\" class=\"btn inner\">Contact Us.</a>.</div>");
			return 'N';
		} else {
			memberEmailCheck = "Y";
			inputMsgHide("companyName3");
		}
		companyName = "Y";
	}
	
	if (formId == "ceoName") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("ceoName", "<div class=\"error-msg\">Enter the name of the CEO.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 30) {
			inputMsgShow("ceoName", "<div class=\"error-msg\">You cannot enter more than 30 characters for CEO name.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		ceoName = "Y";
	}
	
	if (formId == "employeeCount") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("employeeCount", "<div class=\"error-msg\">Enter the number of employees.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		if ((/[^0-9]{1,4}/g).test($("#"+formId).val())) {
			inputMsgShow("employeeCount", "<div class=\"error-msg\">Only numbers can be entered.</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
			//memberAjax("companyPhone");
		}
		
		if ($("#"+formId).val().length > 4) {
			inputMsgShow("employeeCount", "<div class=\"error-msg\">Only up to 4 digits can be entered for the number of employees.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		employeeCount = "Y";
	}
	
	if (formId == "companyPhone" || formId == "companyPhone2") {
		if ($("#"+formId).val() == "") {
			inputMsgShow("companyPhone", "<div class=\"error-msg\">Enter the phone number.</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
		}
		
		if (!(/^[0-9]*$/).test($("#"+formId).val())) {
			inputMsgShow("companyPhone", "<div class=\"error-msg\">Only numbers can be entered.</div>");
			return 'N';
		} else {
			//inputMsgHide(formId);
			if (updateValue != $("#"+formId).val()) {
				memberAjax("companyPhone");
			} else {
				companyPhoneCheck = "Y";
			}
		}
		
		companyPhone = "Y";
	}
	
	if (formId.indexOf("companyAddress") > -1) {
		companyAddress = "N";
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Enter the address.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 100) {
			inputMsgShow(formId, "<div class=\"error-msg\">Up to 100 characters can be entered.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}		
		companyAddress = "Y";
	}
	
	if (formId.indexOf("companyCity") > -1) {
		companyCity = "N";
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Enter the name of the city.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if ($("#"+formId).val().length > 100) {
			inputMsgShow(formId, "<div class=\"error-msg\">Up to 100 characters can be entered.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		companyCity = "Y";
	}
	
	if (formId.indexOf("companyZipcode") > -1) {
		companyZipcode = "N";
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Enter the ZIP code.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		/*if (!(/^[0-9]*$/).test($("#"+formId).val())) {
			inputMsgShow(formId, "<div class=\"error-msg\">Only numbers can be entered.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		var url =  document.location.href.split("/");
		if (url[5] == "en") {
			if ($("#"+formId).val().length  != 4) {
				inputMsgShow("companyZipcode", "<div class=\"error-msg\">Enter the four-digit zip code accurately.</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		} else if (url[5] == "de") {
			if ($("#"+formId).val().length  != 5) {
				inputMsgShow("companyZipcode", "<div class=\"error-msg\">für die PLZ, 5 Zahlen richtig eingeben</div>");
				return 'N';
			} else {
				inputMsgHide(formId);
			}
		} */
		
		
		
		companyZipcode = "Y";
	}

	if (formId.indexOf("zipCode") > -1) {
		zipCode="N";
		// 입력 여부 확인
		if ($("#"+formId).val() == "" || $("#lat").val() == "" || $("#lng").val() == "" ) {
			inputMsgShow(formId, "<div class=\"error-msg\">Enter the address or ZIP code accurately.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		if (!(/^[a-zA-Z0-9\s][a-zA-Z0-9_\-\.\s\,\/]{0,100}$/).test($("#"+formId).val()) ) {
			inputMsgShow(formId, "<div class=\"error-msg\"> Enter the ZIP code.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		zipCode="Y";
	}
	
	if (formId == "preferredDistributor1") {
		if ($("#preferredDistributor1 option:selected").val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Select the name of the preffered distributor.</div>");
			/*inputMsgHide("preferredDistributor2");
			$("#preferredDistributor2").hide();			*/
			return 'N';
		} else if ($("#preferredDistributor1 option:selected").val() == "Other") {
			$("#preferredDistributor2").show();
			inputMsgHide(formId);
		}else{
			inputMsgHide(formId);
			inputMsgHide("preferredDistributor2");
			$("#preferredDistributor2").hide();
			preferredDistributor2= "Y";
		}
		preferredDistributor1= "Y";
	}
	
	if (formId == "preferredDistributor2") {
		// 입력 여부 확인
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Please write the name of your preferred distributor.</div>");
			return 'N';
		} else {
			if($("#"+formId).val().length > 30){
				inputMsgShow(formId, "<div class=\"error-msg\">Enter the 30 .</div>");
				return 'N';
			}else{
				inputMsgHide("preferredDistributor2");
			}
		}
		preferredDistributor2="Y";
	}
	
	if (formId == "memberName") {
		if ($("#"+formId).val() == "") {
			inputMsgShow(formId, "<div class=\"error-msg\">Enter your name.</div>");
			return 'N';
		} else {
			inputMsgHide(formId);
		}
		
		memberName="Y";
	}
	
}

function businessTypeText(checkValue) {
	if (checkValue == "003" || checkValue == "004") {
		$("div[name='companyArea2']").show();
		$("div[name='companyArea1']").hide();
		$("div[name='companyArea1']").find('.error-msg').remove();
		$("div[name='companyArea2']").find('.error-msg').remove();
		
		$("#memberEmail1").attr("placeholder", "Company Email Only");
	} else {
		$("div[name='companyArea1']").show();
		$("div[name='companyArea2']").hide();
		$("div[name='companyArea1']").find('.error-msg').remove();
		$("div[name='companyArea2']").find('.error-msg').remove();
		
		$("#memberEmail1").attr("placeholder", "Company Email Recommended");
		
		if(checkValue == "001"){
			$("#type001").show();
			$("#type002").hide();
		}
		else{
			$("#type002").show();
			$("#type001").hide();
		}
	}
}

function validateMember(addrs) {
	
	//alert($('body,html').scrollTop());
	//alert($("#companyName2 option:selected").val());
	
	memberEmail1 = "N";
	memberEmail2 = "N";
	memberPassword1 = "N";
	memberPassword2 = "N";
	memberCountry = "N";
	companyName = "N";
	ceoName = "N";
	employeeCount = "N";
	companyPhone = "N";
	companyAddress = "N";
	companyCity = "N";
	companyZipcode = "N";
	zipCode = "N";	
	preferredDistributor1 = "N";
	preferredDistributor2 = "N";
	memberName = "N";
	
	if ($("input:radio[name='businessType']:checked").val() == "001" || $("input:radio[name='businessType']:checked").val() == "002") {
		checkForm('memberEmail1');
		checkForm('memberEmail2');
		checkForm('memberPassword1');
		checkForm('memberPassword2');
		checkForm('memberCountry');	
		if ($("input:radio[name='businessType']:checked").val() == "001") { // reseller
			checkForm('companyName1');
			companyCheck = "Y";
		} else { // disty
			checkForm('companyName2');
		}	
		checkForm('ceoName');
		checkForm('employeeCount');
		checkForm('companyPhone');	
		addrs.forEach(function(addr) {
			checkForm(addr);
		});
		/*
		checkForm('companyAddress');
		checkForm('companyCity');
		checkForm('companyZipcode');
		checkForm('zipCode');
		//*/	
		checkForm('preferredDistributor1');
		if($("#preferredDistributor1").val() == "Other"){
			checkForm('preferredDistributor2');
		}
	}
	else{
		checkForm('memberEmail1');
		checkForm('memberEmail2');
		checkForm('memberPassword1');
		checkForm('memberPassword2');
		checkForm('memberName');
		checkForm('companyName3');
	}
	
	
	/*alert("memberEmail1 : " + memberEmail1);
	alert("memberEmail2 : " + memberEmail2);
	alert("memberPassword1 : " + memberPassword1);
	alert("memberPassword2 : " + memberPassword2);
	alert("companyName : " + companyName);
	alert("ceoName : " + ceoName);
	alert("employeeCount : " + employeeCount);
	alert("companyPhone : " + companyPhone);
	alert("companyAddress : " + companyAddress);
	alert("companyCity : " + companyCity);
	alert("companyZipcode : " + companyZipcode);
	alert("memberEmailCheck : " + memberEmailCheck);
	alert("companyPhoneCheck : " + companyPhoneCheck);*/	
	if ($("input:radio[name='businessType']:checked").val() == "001" || $("input:radio[name='businessType']:checked").val() == "002") {
			if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y" && memberCountry == "Y"
				&& companyName == "Y" && ceoName == "Y" && employeeCount == "Y" && companyPhone == "Y"
				&& companyAddress == "Y" && companyCity == "Y" && companyZipcode == "Y" && preferredDistributor1 =="Y" && preferredDistributor2 =="Y" && memberEmailCheck == "Y" && companyPhoneCheck == "Y" && companyCheck == "Y"
			    && zipCode == "Y") {
			
			var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
			$("#memberEmail").val(email);
			var password = $("#memberPassword1").val();
			$("#memberPassword").val(password);
			
			if ($("input:radio[name='businessType']:checked").val() == "001") { // reseller
				$("#companyName").val($("#companyName1").val());
			} else {
				$("#companyName").val($("#companyName2 option:selected").val());
			}
			
			return true;
		} else {
			
			// companyCheck
			
			//$("#companyName1").focus();
			
			if (checkForm('memberEmail1') == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail2') == "N") {
				$("#memberEmail2").focus();
			} else if (memberEmailCheck == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberPassword1') == "N") {
				$("#memberPassword1").focus();
			} else if (checkForm('memberPassword2') == "N") {
				$("#memberPassword2").focus();
			} else if (checkForm('memberCountry') == "N") {
				$("#memberCountry").focus();
			} else if ($("input:radio[name='businessType']:checked").val() == "001" && checkForm('companyName1') == "N") {
				$("#companyName1").focus();
			} else if ($("input:radio[name='businessType']:checked").val() == "002" && checkForm('companyName2') == "N") {
				$("#companyName2").focus();
			} else if ($("input:radio[name='businessType']:checked").val() == "002" && companyCheck == "N") {
				$("#companyTitle").focus();
				$('body,html').animate({
					scrollTop: 400
				}, 0);
			} else if (checkForm('ceoName') == "N") {
				$("#ceoName").focus();
			} else if (checkForm('employeeCount') == "N") {
				$("#employeeCount").focus();
			} else if (checkForm('companyPhone') == "N") {
				$("#companyPhone").focus();
			} 
			/*
			else if (checkForm('companyAddress') == "N") {
				$("#companyAddress").focus();
			} else if (checkForm('companyCity') == "N") {
				$("#companyCity").focus();
			} else if (checkForm('companyZipcode') == "N") {
				$("#companyZipcode").focus();
			} else if (checkForm('zipCode') == "N") {
				$("#zipCode").focus();
			} 
			//*/
			else if (checkForm('preferredDistributor1') == "N") {
				$("#preferredDistributor1").focus();
			} else if ($("#preferredDistributor1").val() == "Other") {
			    if (checkForm('preferredDistributor2') == "N") {
					$("#preferredDistributor2").focus();
				}
			} 
			else{
				addrs.some(function(addr) {
					if(checkForm(addr) == "N"){
						$("#"+ addr).focus();
						return true;
					}
				});
			}
			
			var browser = ieVersion();
			if (Number(browser) > 9) {
				$('body,html').animate({
					scrollTop: $('body,html').scrollTop()-150
				}, 0);
			}
			
			return false;
		}
	}
	else{
		if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y" && memberName == "Y") {
			var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
			$("#memberEmail").val(email);
			var password = $("#memberPassword1").val();
			$("#memberPassword").val(password);
			
			$("#esdCompanyCode").val($("#companyName3 option:selected").val());
			$("#companyName").val($("#companyName3 option:selected").text());
			
			return true;
		}
		else{
			if (memberEmailCheck == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail1') == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail2') == "N") {
				$("#memberEmail2").focus();
			} else if (checkForm('memberPassword1') == "N") {
				$("#memberPassword1").focus();
			} else if (checkForm('memberPassword2') == "N") {
				$("#memberPassword2").focus();
			} else if (checkForm('memberName') == "N") {
				$("#memberName").focus();
			} else if (checkForm('companyName3') == "N") {
				$("#companyName3").focus();
			}
			
			var browser = ieVersion();
			if (Number(browser) > 9) {
				$('body,html').animate({
					scrollTop: $('body,html').scrollTop()-150
				}, 0);
			}
			
			return false;
		}
	}	
}

function validateMember2(addrs) {
	
	memberEmail1 = "N";
	memberEmail2 = "N";
	memberPassword1 = "N";
	memberPassword2 = "N";
	memberCountry = "N";
	companyName = "N";
	ceoName = "N";
	employeeCount = "N";
	companyPhone = "N";
	companyAddress = "N";
	companyCity = "N";
	companyZipcode = "N";
	zipCode = "N";	
	
	memberEmailCheck = "N";
	companyPhoneCheck = "N";
	companyCheck = "N";
	
	checkForm('memberEmail1');
	checkForm('memberEmail2', $("input[name='memberEmail']").val());
	if ($("#memberPassword1").val() != "" || $("#memberPassword2").val() != "" ) {
        checkForm('memberPassword1', 'Y');
        checkForm('memberPassword2', 'Y');
    } else {
        memberPassword1 = "Y";
        memberPassword2 = "Y";
    }
	checkForm('memberCountry');	
	if ($("input[name='businessType']").val() == "001") { // reseller
		checkForm('companyName1');
		companyCheck = "Y";
	} else { // disty
		companyCheck = "Y";
		companyName = "Y";
	}	
	checkForm('ceoName');
	checkForm('employeeCount');
	checkForm('companyPhone', $("input[name='companyPhone']").val());	
	addrs.forEach(function(addr) {
		checkForm(addr);
	});

	if ($("input[name='businessType']").val() == "001" || $("input[name='businessType']").val() == "002") {
			if (memberEmail1 == "Y" && memberEmail2 == "Y" && memberPassword1 == "Y" && memberPassword2 == "Y" && memberCountry == "Y"
				&& companyName == "Y" && ceoName == "Y" && employeeCount == "Y" && companyPhone == "Y"
				&& companyAddress == "Y" && companyCity == "Y" && companyZipcode == "Y" && memberEmailCheck == "Y" && companyPhoneCheck == "Y" && companyCheck == "Y"
			    && zipCode == "Y") {
			
			var email = $("#memberEmail1").val() + "@" + $("#memberEmail2").val();
			$("#memberEmail").val(email);
			var password = $("#memberPassword1").val();
			$("#memberPassword").val(password);
			
			if ($("input[name='businessType']").val() == "001") { // reseller
				$("#companyName").val($("#companyName1").val());
			} else {
				$("#companyName").val($("#companyName2 option:selected").val());
			}
			
			return true;
		} else {

			if (checkForm('memberEmail1') == "N") {
				$("#memberEmail1").focus();
			} else if (checkForm('memberEmail2', $("input[name='memberEmail']").val()) == "N") {
				$("#memberEmail2").focus();
			} else if (memberEmailCheck == "N") {
				$("#memberEmail1").focus();
			} else if ($("#memberPassword1").val() != "" || $("#memberPassword2").val() != "" ) {
	            
	            if ($("#memberPassword1").val() != "" || $("#memberPassword2").val() != "" ) {
	                
	                if (checkForm('memberPassword1') == "N") {
	                    $("#memberPassword1").focus();
	                } else if (checkForm('memberPassword2') == "N") {
	                    $("#memberPassword2").focus();
	                }
	            } else {
	                memberPassword1 = "Y";
	                memberPassword2 = "Y";
	            }
			} else if (checkForm('memberCountry') == "N") {
				$("#memberCountry").focus();
			} else if ($("input[name='businessType']").val() == "001" && checkForm('companyName1') == "N") {
				$("#companyName1").focus();
			} else if ($("input[name='businessType']").val() == "002" && checkForm('companyName2') == "N") {
				$("#companyName2").focus();
			} else if ($("input[name='businessType']").val() == "002" && companyCheck == "N") {
				$("#companyTitle").focus();
				$('body,html').animate({
					scrollTop: 400
				}, 0);
			} else if (checkForm('ceoName') == "N") {
				$("#ceoName").focus();
			} else if (checkForm('employeeCount') == "N") {
				$("#employeeCount").focus();
			} else if (checkForm('companyPhone', $("input[name='companyPhone']").val()) == "N") {
				$("#companyPhone").focus();
			} 
			else{
				addrs.some(function(addr) {
					if(checkForm(addr) == "N"){
						$("#"+ addr).focus();
						return true;
					}
				});
			}
			
			var browser = ieVersion();
			if (Number(browser) > 9) {
				$('body,html').animate({
					scrollTop: $('body,html').scrollTop()-150
				}, 0);
			}
			
			return false;
		}
	}
}

function memberAjax(selectWhere) {	
	
	var param = "";
	if (selectWhere == "memberEmail") {
		param = "memberEmail="+$("#memberEmail1").val() + "@" + $("#memberEmail2").val();
	} else if (selectWhere == "companyPhone") {
		param = "companyPhone="+$("#companyPhone").val();
	}
	
	$.ajax({
        type: 'POST',
        url: '/eu/membership/memberFormCheck.lg',
        data : param + '&selectWhere='+selectWhere,
        dataType : 'xml',
        async:false,
        success: function(json){
        	
        	var result = $(json).find("result").text();
        	var selectWhere = $(json).find("selectWhere").text();
        	// 이메일 중복 체크
        	if (selectWhere == "memberEmail") {
        		if (result == 0) {
        			inputMsgHide("memberEmail1");
        			memberEmailCheck = "Y";
        		} else {
        			var altContent = "";
        			altContent += "<div class=\"error-msg\">There is already an account. Please enter another email address.";
        			altContent += "</div>";
        			inputMsgShow("memberEmail1", altContent);
        			memberEmailCheck = "N";
        		}
        	} else if (selectWhere == "companyPhone") {
        		if (result == 0) {
        			inputMsgHide("companyPhone");
        			companyPhoneCheck = "Y";
        		} else {
        			var memberEmail = $(json).find("memberEmail").text();
        			memberEmail = memberEmail.substring(0,1) + "**********";
        			
        			var altContent = "";
        			altContent+= "<div class=\"error-msg\">This phone number has already been joined by "+memberEmail+"";
        			altContent+= "	<a href=\"javascript:fnLogin();\">Login</a>";
        			altContent+= "	<a href=\"javascript:fnResetPwd();\" class=\"btn inner\">Reset password</a>";
        			altContent+= "</div>";
        			
        			inputMsgShow("companyPhone", altContent);
        			companyPhoneCheck = "N";
        		}
        	}
        },
        error:function (e){
        	alert(e.responseText);
        }
 	});
}

function companyAjax() {	
	
	$.ajax({
        type: 'POST',
        url: '/eu/membership/memberFormCompanyCheck.lg',
        data : 'companyName='+$("#companyName2").val(),
        dataType : 'xml',
        async:false,
        success: function(json){
        	
        	var result = $(json).find("result").text();
        	var companySeq = $(json).find("seq").text();
        	var companyEmail = $(json).find("memberEmail").text();
        	if (companySeq == "") {
        		inputMsgHide("companyName2");
        		companyCheck = "Y";
        	} else {
        		var altContent = "";
    			altContent += "<div class=\"error-msg\">Selected companies are already inducted (id : "+companyEmail+")";
    			altContent += "	<a href=\"javascript:fnLogin();\" class=\"btn inner\">Login</a>";
    			altContent += "	<a href=\"javascript:fnContactus();\" class=\"btn inner\">Contact Us</a>";
    			altContent += "</div>";
        		inputMsgShow("companyName2", altContent);
        		companyCheck = "N";
        	}
        },
        error:function (e){
        	alert(e.responseText);
        }
 	});
}