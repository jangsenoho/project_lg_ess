function dateFormat(date, lang){
    tmp = date.split(".");
    result = "-";
    if(tmp.length > 2)
    	if(lang == "au" || lang == "eu" || lang == "it" || lang == "es" || lang == "de")
    		result = tmp[2]+'.'+tmp[1]+'.'+tmp[0];
    	else
    		result = tmp[1]+'.'+tmp[2]+'.'+tmp[0];
    		    		
    return result;		
};


function EDSopen(ID,language){
	window.open("/eds?&id="+ID+"&languageCode="+language,"","location=yes, scrollbars=yes, toolbar=yes, menubar=yes, height=" +screen.height+", width=" +screen.width);
}