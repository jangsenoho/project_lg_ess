var productSwiper;

$(function(){
    // GNB mouse hover
    $('.gnb').on('mouseenter',function(){
         $('.l-header-wrap').addClass('is-hover');
    }).on('mouseleave',function(){
        $('.l-header-wrap').removeClass('is-hover');
    });

    // Language Select
    $('.language-area').on('click',function(){
        $(this).toggleClass('is-select');
        if($(this).siblings('.partner-area').hasClass('is-grade')){
            $(this).siblings('.partner-area.is-login').removeClass('is-grade');
        }
    });

    // User Info Button
    $('.partner-area .user').on('click',function(){
        $(this).parents('.partner-area').toggleClass('is-grade');
        if($(this).parents('.partner-area.is-login').siblings('.language-area').hasClass('is-select')){
            $(this).parents('.partner-area.is-login').siblings('.language-area').removeClass('is-select');
        }
    });

    var $window = $(window);
    var winH = $window.outerHeight();
    var wrapH = $('.l-wrap').outerHeight();
    var $quickMenu = $('.l-quick-menu');
    var quickMenuH = $('.l-quick-menu').outerHeight();
    var totalH = wrapH - winH;

    $window.on('resize',function(){
        winH = $window.outerHeight();
        wrapH = $('.l-wrap').outerHeight();
        totalH = wrapH - winH;
    });
    $window.on('scroll',function(){
        winH = $window.outerHeight();
        wrapH = $('.l-wrap').outerHeight();
        totalH = wrapH - winH;
        var st = $(this).scrollTop();
        if(st > totalH - 80){
            $quickMenu.addClass('nofloating');
        }else{
            $quickMenu.removeClass('nofloating');
        }

        if(st > 0){
            $('.btn-top').addClass('fadeIn');
        }else{
            $('.btn-top').removeClass('fadeIn');
        }
    });

    // Scroll bar
    if($('.agree-info').length > 0){    
        $('.agree-info').mCustomScrollbar({
            theme:"dark"
        });
    }

    // Layer POPUP
    $('.btn-popup').on('click',function(){
        var $layerName = $(this).attr('data-layer');
        $('.l-popup-wrap[data-layer="'+$layerName+'"]').css('display','flex').append('<div class="dim"></div>');
        $('body').addClass('is-overflow');
        if($('.product-swiper').length > 0){
            productSwiper = new Swiper('.product-swiper', {
                autoHeight: true, //enable auto height
                loop:true,
                navigation: {
                    nextEl: '.product-next',
                    prevEl: '.product-prev',
                },
                simulateTouch:false,
            });
            $('.product-info-area').mCustomScrollbar({
                theme:"dark"
            });
        }
    });
    $('.popup-close').on('click',function(){
        $(this).parents('.l-popup-wrap').hide().find('.dim').remove();
        $('body').removeClass('is-overflow');
        if($('.product-swiper').length > 0){
            productSwiper.destroy();
        }
    });
    $(document).on('click','.dim',function(){
        $('.popup-close').trigger('click');
    });
    // Accumulated Points popup 닫기
    $('[data-layer="accumulated-points"').on('click','.btn-black',function(){
        $('.popup-close').trigger('click');
    });

    // Tooltip
    $('.info-area').on('click','button',function(){
        if($(this).hasClass('tooltip-show')){
            $(this).parents('.info-area').find('.tooltip-box').show();
        }else{
            $(this).parents('.tooltip-box').hide();
        }
    });

    // Inquiry Accordion
    $('.accordion-wrap').on('click','.accordion-select',function(){
        if($(this).hasClass('questions')){
            if($(this).parents('.list').hasClass('complete')){    
                if(!$(this).parents('.list').hasClass('is-toggle')){
                    $(this).parents('.list').addClass('is-toggle');
                }else{
                    $(this).parents('.list').removeClass('is-toggle');
                }
            }
        }else{
            if(!$(this).parents('.list').hasClass('is-toggle')){
                $(this).parents('.list').addClass('is-toggle');
            }else{
                $(this).parents('.list').removeClass('is-toggle');
            }
        }
    });
    
    // Related Site
    $('.related-area').on('click',function(){
        $(this).toggleClass('is-open');
    });

    // Scroll Top
    $('.btn-top').on('click',function(){
        $('html,body').stop().animate({
            scrollTop : 0,
        }, 500);
    });

    //datepicker
    var datePicker = $('.datepicker');
    if(datePicker.length > 0){
        datePicker.children('input[type="text"]').datepicker({
            dateFormat: "dd.mm.yy"
        });
        datePicker.on('click','button',function(){
            $(this).prev('input').focus();
        });
    }

    // Product View Change
    if($('.view-change').length > 0){
        $('.view-change').on('click', 'button' ,function(){
            $(this).addClass('is-show').siblings().removeClass('is-show');
            if($(this).hasClass('view-thumbnail')){
                $(this).parents('.products').find('.products-wrap').removeClass('show-list');
            }
            if($(this).hasClass('view-list')){
                $(this).parents('.products').find('.products-wrap').addClass('show-list');
            }
        });
    }
    
    // Password Text Show/Hidden
    $('.password-visibility').on('click',function(){
        $(this).toggleClass('show');
        var $password = $(this).parents('.password').find('input');
        if ($password.attr('type') === "password") {
            $password.attr('type','text');
        } else {
            $password.attr('type','password');
        }
    });
});